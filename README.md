#ArcGIS Map
项目安装
项目基于maven，采用spring boot构建简单web。<br/>
端口等修改可在application.properties中修改（具体搜索spring boot 配置）。<br/>
运行 SampleWebJspApplication 即可启动web服务。<br/>
1、百度地图因为每层分辨率不同，而且不遵从标准规范，可见welcome.html<br/>
2、天地图 tdt.html显示了天地图底图<br/>
3、天地图影像  tdtImg.html显示了天地图影像，加载了anno图层<br/>