/// <reference path="Easyui/jquery-1.7.2.min.js" />

//Add by mercy@20150417 统计站点超标、正常、停产
var StatisticsEnterpriseTool = {
    StatisticsItems: null, //统计项目
    //Add by mercy@20150421am 获取站点统计信息
    GetStatisticsEnterprise: function (jsonData) {
        /*
        * 站点异常-5
        * 正常-0
        * 超标-显示超标的值
        * 停产- -1
        * 故障- -2 
        */
        var NormalCount = 0; //正常
        var OverCount = 0; //超标
        var StopCount = 0; //停产
        var MalfunctionCount = 0; //故障
        var array = {};

        if (jsonData.length == 0) {
            array.NormalCount = NormalCount;
            array.OverCount = OverCount;
            array.StopCount = StopCount;
            array.MalfunctionCount = MalfunctionCount;
            return array;
        }
        if (arguments.length == 2) {
            dojo.forEach(jsonData, function (item) {
                switch (item.value) {
                    case "0":
                        NormalCount++;
                        break;
                    case "-1":
                        StopCount++;
                        break;
                    case "-2":
                        MalfunctionCount++;
                        break;
                    case "-5":
                        //程序异常的情况
                        break;
                    default:
                        OverCount++;
                        break;
                }
            });
        }
        else {
            dojo.forEach(jsonData, function (item) {
                switch (item.lever.value) {
                    case "0":
                        NormalCount++;
                        break;
                    case "-1":
                        StopCount++;
                        break;
                    case "-2":
                        MalfunctionCount++;
                        break;
                    case "-5":
                        //程序异常的情况
                        break;
                    default:
                        OverCount++;
                        break;
                }
            });
        }

        array.NormalCount = NormalCount;
        array.OverCount = OverCount;
        array.StopCount = StopCount;
        array.MalfunctionCount = MalfunctionCount;
        return array;
    },
    StatisticsEnterpriseCount: function (jsonData, objcode) {
        var array = StatisticsEnterpriseTool.GetStatisticsEnterprise(jsonData);
        StatisticsEnterpriseTool.ShowStatisticsCount(array, objcode);
    },
    ShowStatisticsCount: function (item, objcode) {
        var _vv = '';
        //if (objcode == "0") {
        //    var _count = item.NormalCount + item.OverCount + item.MalfunctionCount + item.StopCount;
        //    //_vv = '超标：' + item.OverCount + ' 总数：' + _count;
        //    //_vv = '<img src="images/mark/GreenPin1LargeB.png" /><span>' + item.OverCount + '</span>'
        //    //    + '<span style=" margin-left:15px;"> 总数：</span><span>' + _count + '</span>';
        //}
        //else {
        //    //_vv = '正常：' + item.NormalCount + ' 超标：' + item.OverCount + ' 故障：' + item.MalfunctionCount + ' 停产：' + item.StopCount;
        //    //_vv = '<img src="images/mark/GreenPin1LargeB.png" /><span>' + item.NormalCount + '</span>'
        //    //    + '<img src="images/mark/RedPin1LargeB.png" /><span>' + item.OverCount + '</span>'
        //    //    + '<img src="images/mark/YellowPin1LargeB.png" /><span>' + item.MalfunctionCount + '</span> '
        //    //    + '<img src="images/mark/GrayPin1LargeB.png" /><span>' + item.StopCount + '</span>';
        //    _vv = '<img src="images/mark/GreenPin1LargeB.png" /><span>正常</span>'
        //        + '<img src="images/mark/RedPin1LargeB.png" /><span>超标</span>'
        //        + '<img src="images/mark/GrayPin1LargeB.png" /><span>停产</span>'
        //        + '<img src="images/mark/YellowPin1LargeB.png" /><span>故障</span> ';
        //}
        _vv = '<img src="images/mark/GreenPin1LargeB.png" /><span>正常</span>'
                + '<img src="images/mark/RedPin1LargeB.png" /><span>超标</span>'
                + '<img src="images/mark/GrayPin1LargeB.png" /><span>停产</span>'
                + '<img src="images/mark/YellowPin1LargeB.png" /><span>故障</span> ';


        var _mapW = $("#Mymap").width();
        var _mapH = $("#Mymap").height();
        var _div_statistics_tip = '<div id="div_statistics_tip" >' + _vv + '</div>';
        $("#div_statistics_tip").remove();
        $("#Mymap").append(_div_statistics_tip);

        //if (objcode == "0") {
        //    $("#div_statistics_tip").css('width', '170px');
        //}
        //else {
        //    $("#div_statistics_tip").css('width', '270px');
        //}
        $("#div_statistics_tip").css('width', '270px');
    },
    //显示AQI图例
    ShowLegend: function () {
        var _vv = '<img src="images/mark/GreenPin1LargeB.png" /><span>正常</span>'
                + '<img src="images/mark/RedPin1LargeB.png" /><span>超标</span>'
                + '<img src="images/mark/GrayPin1LargeB.png" /><span>停产</span>'
                + '<img src="images/mark/YellowPin1LargeB.png" /><span>故障</span> ';

        var _mapW = $("#Mymap").width();
        var _mapH = $("#Mymap").height();
        var _div_statistics_tip = '<div id="div_statistics_tip" >' + _vv + '</div>';
        $("#div_statistics_tip").remove();
        $("#Mymap").append(_div_statistics_tip);
        $("#div_statistics_tip").css('width', '248px');
        $("#div_statistics_tip").css('position', 'absolute');
        $("#div_statistics_tip").css('right', '0');
        $("#div_statistics_tip").css('bottom', '40px');
        $("#div_statistics_tip").css('vertical-align', 'middle');
    },
    HideLegend: function () {
        $("#div_statistics_tip").hide();
    }
};