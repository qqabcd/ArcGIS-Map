/// <reference path="Easyui/jquery-1.7.2.min.js" />
/// <reference path="MainMapCommonT.js" />

var TreeMenu = {
    SelectNodeId: "",
    TreeRequestUrl: null,
    PreExpandNode: null, //记录上个展开的节点
    isTreeClick: true,
    InitTree: function (cityCode, objType) {
		if (cityCode=="370100")
        {
            cityCode += ",371200";
        }
        $("#menutree").tree({
            lines: true,
            url: this.TreeRequestUrl + "?Method=GetStationTreeNode&cityCode=" + cityCode + "&Loadtype=" + objType,
            onBeforeLoad: function (node, param) {
                param.Method = "LoadTree";

                if (node) {
                    param.NodeID = node.id;
                    if (node.attributes) {
                        param.ParentNodeType = node.attributes.type;
                    }
                }
                if (GetCompany.hasOwnProperty("isRoot") && !GetCompany.isRoot) {
                    SetLoadingImg("progressBar", "", 350, 60, "", "show");
                }
            },
            onLoadSuccess: function (node, data) {

                //if (data != null || data != "" || data != undefined) {
                if (!node) {
                    var roots = $("#menutree").tree("getRoots");
                    for (var i = 0; i < roots.length; i++) {
                        $("#menutree").tree("expand", roots[i].target);
                    }
                    TreeMenu.PreExpandNode = null;
                } else {
                    if (!node.id) return;
                    if (this.SelectNodeId == node.id) {
                        var _stationID = data[0].id;
                        GetCompany.GetStationInfomationByID(_stationID);
                    }
                }

                setTimeout(function () {
                    SetLoadingImg("progressBar", "", 350, 60, "", "hide");
                }, 500);
            },
            onSelect: function (node) {
                if (!node || !node.attributes || !node.attributes.type) {
                    return;
                }
                if (!TreeMenu.isTreeClick) return; //不是在tree上做的点击，说明是在gis地图做的点击，不需要重新执行这个事件
                // if (node.state == "open") {
                //     $("#menutree").tree("collapse", node.target);
                // }
                // else if (node.state == "closed") {

                $(".stationNode").each(function (index, item) {
                    $(item).removeClass("stationNode");
                });

                var _hasSon = node.attributes.hasSon;
                switch (_hasSon) {
                    case "yes":
                        if (node.state == "open") {
                            $("#menutree").tree("collapse", node.target);
                        } else {
                            $("#menutree").tree("expand", node.target);
                            var _nodeSelect = $("#menutree").tree("getChildren", node.target);
                            if (_nodeSelect.length == 0) {
                                this.SelectNodeId = node.id;
                            } else {
                                GetCompany.GetStationInfomationByID(_nodeSelect[0].id);
                            }
                        }
                        break;
                    case "no":
                        if (TreeMenu.PreExpandNode) {
                            $('#menutree').tree('collapse', TreeMenu.PreExpandNode.target);

                        }
                        $(node.target).addClass("stationNode");
                        GetCompany.GetStationInfomationByID(node.id, "no");
                        break;
                }
                //}

                if (node.attributes.type == "area") {

                } else if (node.attributes.type == "enterprise") {

                } else if (node.attributes.type == "station") {
                    var _stationID = node.id;
                    GetCompany.GetStationInfomationByID(_stationID);
                }


            },
            onExpand: function (node) {
                if (!node || !node.attributes || !node.attributes.type) {
                    return;
                }
                var _hasSon = node.attributes.hasSon;
                if (_hasSon === "yes") {
                    //先清除历史设定的高亮样式
                    $(".stationNode").each(function (index, item) {
                        $(item).removeClass("stationNode");
                    });
                    //先关闭之前展开的节点
                    if (TreeMenu.PreExpandNode && TreeMenu.PreExpandNode.id != node.id) {
                        $('#menutree').tree('collapse', TreeMenu.PreExpandNode.target);

                    }
                    TreeMenu.PreExpandNode = node;
                    //获取此节点的子节点
                    var _nodeSelect = $("#menutree").tree("getChildren", node.target);
                    if (_nodeSelect.length > 0) {
                        for (var i = 0; i < _nodeSelect.length; i++) {
                            $(_nodeSelect[i].target).addClass("stationNode"); //增加高亮样式
                        }
                    }
                }
            }
        });
    }
};
