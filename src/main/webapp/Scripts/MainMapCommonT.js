/// <reference path="Easyui/jquery-1.7.2.min.js" />  
/// <reference path="TreeMenu.js" />
/// <reference path="GlobalJs.js" />


//获取企业信息
var GetCompany = {
    GetCurrentSelect: function () {
        var _menuCurrentSelected = "";
        $(".top_dh >ul >li").each(function (index, doElem) {
            var _src = $(doElem).find("img").attr("src");
            if (_src.indexOf("_hover") > -1) {
                _menuCurrentSelected = $(doElem).find("img").attr("id");
                return false;
            }
            else {
                _menuCurrentSelected = "WasteWaterGis";
            }
        });

        return _menuCurrentSelected;
    },
    GetCompanyList: function () {
        if (GetCompany.hasOwnProperty("isRoot") && !GetCompany.isRoot) {
            SetLoadingImg("progressBar", 60, 350, "", "", "show");
        }

        if (arguments.length == 1) {
            GetCompany.GetCompanyListByCityCode(arguments[0]);
            //return; //Add by mercy@20150418 解决重复加载的问题
        }
        else {
            //var _selectedCity = CityListSelectedStyle.GetCityListSelected();
            //GetCompany.GetCompanyListByCityCode(_selectedCity);

            MenuContrl.SetMenuCityHide();
            GetCompany.GetCompanyListCountLayer(); //Add by mercy@20150418 解决重复加载的问题
        }

        //        var _menuCurrentSelected = GetCompany.GetCurrentSelect();
        //        switch (_menuCurrentSelected) {
        //            case "WasteWaterGis":
        //                GetCompany.GetCompanyList_ZDFS();
        //                break;
        //            case "WasteAirGis":
        //                GetCompany.GetCompanyList_ZDFQ();
        //                break;
        //            case "SewagePlantGis":
        //                GetCompany.GetCompanyList_WSC();
        //                break;
        //            default:
        //                GetCompany.GetCompanyList_ZDFS();
        //                break;
        //        }

    },
    //Add by mercy@20150420pm 获取城市企业统计图层信息
    GetCompanyListCountLayer: function () {
        var _menuCurrentSelected = GetCompany.GetCurrentSelect();
        switch (_menuCurrentSelected) {
            case "WasteWaterGis":
                GetCompany.GetCompanyList_ZDFS();
                break;
            case "WasteAirGis":
                GetCompany.GetCompanyList_ZDFQ();
                break;
            case "SewagePlantGis":
                GetCompany.GetCompanyList_WSC();
                break;
            default:
                GetCompany.GetCompanyList_ZDFS();
                break;
        }
    },
    GetCompanyListByCityCode: function (cityCode) {

        if (GetCompany.hasOwnProperty("isRoot") && !GetCompany.isRoot) {
            SetLoadingImg("progressBar", 60, 350, "", "", "show");
        }
        var objType = GetCompany.GetCurrentSelect();
        switch (objType) {
            case "WasteWaterGis":
                GetCompany.GetCompanyListByCityCode_ZDFS(cityCode);
                break;
            case "WasteAirGis":
                GetCompany.GetCompanyListByCityCode_ZDFQ(cityCode);
                break;
            case "SewagePlantGis":
                GetCompany.GetCompanyListByCityCode_WSC(cityCode);
                break;
            default:
                GetCompany.GetCompanyListByCityCode_ZDFS(cityCode);
                break;
        }
    },
    GetCompanyList_ZDFS: function () {
        $.ajax({
            type: "post",
            dataType: "json",
            url: "ajax/WasteWaterGis/WasteWaterHandler.ashx",
            data: {
                Method: "GetCompanyList_New",
                isall: !MapSetting.hasOwnProperty("isAll") ? "0" : MapSetting.isAll
            },
            success: function (data) {
                if (data == "") {
                    return;
                }

                /*
                var _heightRight = $("#right").height();
                var _heighBot = $("#bot").height();


                var _arrayData = data.split("$$");
                SetTotailDiv(_arrayData[1]);

                var _divTotailH = $("#div_totail").height();
                var _listH = parseInt(_heightRight) - parseInt(_heighBot) - parseInt(_divTotailH);
                $("#divList").height(_listH);
                SetValue("divList", _arrayData[0]);

                //初始化17地市下的站点个数
                CityLayer.SetStationCountLayer(_arrayData[2]);

                //senfe("senfe", "#EAF7FF", "#E5F3FE", "#d4eeff", "#d4eeff"); //#1088d7
                //senfe("senfe", "#EAF7FF", "#E5F3FE", "#97dbf2", "#97dbf2");
                senfe("senfe", "#fff", "#f1f4f8", "#badde9", "#badde9");
                */
                //var _arrayData = data.split("$$");
                //初始化17地市下的站点个数
                //CityLayer.SetStationCountLayer(_arrayData[2]);
                //GetCompany.GetCompanyListByCityCode("370100");

                CityLayer.SetStationCountLayer_New(data);
                SetLoadingImg("progressBar", "", "", "", "", "hide");
            },
            error: function (data) {
                SetLoadingImg("progressBar", "", "", "", "", "hide");
                //alert(data);
            }
        });
    },
    GetCompanyList_ZDFQ: function () {
        $.ajax({
            type: "post",
            dataType: "json",
            url: "ajax/WasteAirGis/WasteAirHandler.ashx",
            data: {
                Method: "GetCompanyList_New",
                isall: !MapSetting.hasOwnProperty("isAll") ? "0" : MapSetting.isAll
            },
            success: function (data) {
                if (data == "") return;
                /*
                var _heightRight = $("#right").height();
                var _heighBot = $("#bot").height();


                var _arrayData = data.split("$$");
                SetTotailDiv(_arrayData[1]);

                var _divTotailH = $("#div_totail").height();
                var _listH = parseInt(_heightRight) - parseInt(_heighBot) - parseInt(_divTotailH);
                $("#divList").height(_listH);
                SetValue("divList", _arrayData[0]);

                //初始化17地市下的 站点个数
                CityLayer.SetStationCountLayer(_arrayData[2]);

                //senfe("senfe", "#EAF7FF", "#E5F3FE", "#d4eeff", "#d4eeff"); //#1088d7
                //senfe("senfe", "#EAF7FF", "#E5F3FE", "#97dbf2", "#97dbf2");
                senfe("senfe", "#fff", "#f1f4f8", "#badde9", "#badde9");
                */
                //var _arrayData = data.split("$$");
                //初始化17地市下的站点个数
                //CityLayer.SetStationCountLayer(_arrayData[2]);
                //GetCompany.GetCompanyListByCityCode("370100");

                CityLayer.SetStationCountLayer_New(data);
                SetLoadingImg("progressBar", "", "", "", "", "hide");
            },
            error: function (data) {
                SetLoadingImg("progressBar", "", "", "", "", "hide");
                //alert(data);
            }
        });
    },
    GetCompanyList_WSC: function () {
        $.ajax({
            type: "post",
            dataType: "json",
            url: "ajax/SewagePlantGis/SewagePlantHandler.ashx",
            data: {
                Method: "GetCompanyList_New",
                isall: !MapSetting.hasOwnProperty("isAll") ? "0" : MapSetting.isAll
            },
            success: function (data) {
                if (data == "") return;

                /*
                var _heightRight = $("#right").height();
                var _heighBot = $("#bot").height();


                var _arrayData = data.split("$$");
                SetTotailDiv(_arrayData[1]);

                var _divTotailH = $("#div_totail").height();
                var _listH = parseInt(_heightRight) - parseInt(_heighBot) - parseInt(_divTotailH);
                $("#divList").height(_listH);
                SetValue("divList", _arrayData[0]);


                //初始化17地市下的站点个数
                CityLayer.SetStationCountLayer(_arrayData[2]);

                //senfe("senfe", "#EAF7FF", "#E5F3FE", "#d4eeff", "#d4eeff"); //#1088d7
                //senfe("senfe", "#EAF7FF", "#E5F3FE", "#97dbf2", "#97dbf2");
                senfe("senfe", "#fff", "#f1f4f8", "#badde9", "#badde9");
                */
                //var _arrayData = data.split("$$");
                //初始化17地市下的站点个数
                //CityLayer.SetStationCountLayer(_arrayData[2]);
                //GetCompany.GetCompanyListByCityCode("370100");

                CityLayer.SetStationCountLayer_New(data);
                SetLoadingImg("progressBar", "", "", "", "", "hide");
            },
            error: function (data) {
                SetLoadingImg("progressBar", "", "", "", "", "hide");
                //alert(data);
            }
        });
    },
    GetCompanyListByCityCode_ZDFS: function (cityCode) {

        TreeMenu.TreeRequestUrl = "ajax/WasteWaterGis/WasteWaterHandler.ashx";
        TreeMenu.InitTree(cityCode, "WasteWaterGis");


        //SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        /*$.ajax({
        type: "post",
        url: "ajax/WasteWaterGis/WasteWaterHandler.ashx",
        data: { Method: "GetCompanyCountByCityCode", CityCode: cityCode },
        success: function (data) {
        if (data == "") return;
        var _arrayData = data.split("$$");
        SetValue("divList", _arrayData[0]);
        SetTotailDiv(_arrayData[1]);

        //senfe("senfe", "#EAF7FF", "#E5F3FE", "#d4eeff", "#d4eeff");
        //senfe("senfe", "#EAF7FF", "#E5F3FE", "#97dbf2", "#97dbf2");
        senfe("senfe", "#fff", "#f1f4f8", "#badde9", "#badde9");
        SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        },
        error: function (data) {
        SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        //alert(data);
        }
        });*/
    },
    GetCompanyListByCityCode_ZDFQ: function (cityCode) {
        TreeMenu.TreeRequestUrl = "ajax/WasteAirGis/WasteAirHandler.ashx";
        TreeMenu.InitTree(cityCode, "WasteAirGis");
        //SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        /*$.ajax({
        type: "post",
        url: "ajax/WasteAirGis/WasteAirHandler.ashx",
        data: { Method: "GetCompanyCountByCityCode", CityCode: cityCode },
        success: function (data) {
        if (data == "") return;
        var _arrayData = data.split("$$");
        SetValue("divList", _arrayData[0]);
        SetTotailDiv(_arrayData[1]);

        //senfe("senfe", "#EAF7FF", "#E5F3FE", "#d4eeff", "#d4eeff");
        //senfe("senfe", "#EAF7FF", "#E5F3FE", "#97dbf2", "#97dbf2");
        senfe("senfe", "#fff", "#f1f4f8", "#badde9", "#badde9");
        SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        },
        error: function (data) {
        SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        //alert(data);
        }
        }); */
    },
    GetCompanyListByCityCode_WSC: function (cityCode) {
        TreeMenu.TreeRequestUrl = "ajax/SewagePlantGis/SewagePlantHandler.ashx";
        TreeMenu.InitTree(cityCode, "SewagePlantGis");
        //SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        /*$.ajax({
        type: "post",
        url: "ajax/SewagePlantGis/SewagePlantHandler.ashx",
        data: { Method: "GetCompanyCountByCityCode", CityCode: cityCode },
        success: function (data) {
        if (data == "") return;
        var _arrayData = data.split("$$");
        SetValue("divList", _arrayData[0]);
        SetTotailDiv(_arrayData[1]);

        //senfe("senfe", "#EAF7FF", "#E5F3FE", "#d4eeff", "#d4eeff");
        //senfe("senfe", "#EAF7FF", "#E5F3FE", "#97dbf2", "#97dbf2");
        senfe("senfe", "#fff", "#f1f4f8", "#badde9", "#badde9");
        SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        },
        error: function (data) {
        SetLoadingImg("progressBar", "", 350, 60, "", "hide");
        //alert(data);
        }
        }); */
    },
    GetStationInfomationByID: function (stationID) {

        var _paramerLen = arguments.length;
        var _currentType = this.GetCurrentSelect();
        var _currentUrl = "";
        switch (_currentType) {
            case "WasteWaterGis":
                _currentUrl = 'ajax/WasteWaterGis/WasteWaterHandler.ashx';
                break;
            case "WasteAirGis":
                _currentUrl = 'ajax/WasteAirGis/WasteAirHandler.ashx';
                break;
            case "SewagePlantGis":
                _currentUrl = 'ajax/SewagePlantGis/SewagePlantHandler.ashx';
                break;
            default:
                _currentUrl = 'ajax/WasteWaterGis/WasteWaterHandler.ashx';
                break;
        }

        $.ajax({
            type: "post",
            url: "ajax/map.ashx",
            data: {
                Method: "SelectSubList",
                StationID: stationID,
                stcode: "0",
                type: _currentType,
                isall: !MapSetting.hasOwnProperty("isAll") ? "0" : MapSetting.isAll
            },
            success: function (data) {
                if (data == "") return;
                var _json = eval("(" + data + ")");

                if (_json.items[0] == undefined) {
                    var _msg = '<div style=" width:250px; margin-top:20px; font-family:宋体; font-weight:bold;">'
                        + '该站点暂无坐标信息，无法定位。'
                        + '</div>';
                    $.messager.alert('温馨提示', _msg, 'info');
                    return;
                }
                var url = _json.items[0].url;
                var entCode = _json.items[0].enterpriseCode;
                var pname = _json.items[0].pname;
                var subname = _json.items[0].subname;
                var subid = _json.items[0].subid;
                var stcode = _json.items[0].stcode;
                var type = _json.items[0].type;
                var longitude = _json.items[0].longitude;
                var latitude = _json.items[0].latitude;
                var lever = _json.items[0].lever;
                var tempid = _json.items[0].tempid;

                //处理infowindow框显示的名字
                var _showName = '';
                if (_paramerLen == 2) {
                    _showName = pname;
                }
                else {
                    _showName = pname + subname;
                }

                var point = new esri.geometry.Point(parseFloat(longitude), parseFloat(latitude)); //创建点
                map.centerAndZoom(point, 10);

                var content = "";
                if (lever.value > GraphicMarkImg.st_low) {
                    content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=high&StrCode=' + stcode + "&Tempid=" + tempid + "&Pname=" + escape(pname) + '&SubId=' + subid + '&SubName=' + escape(_showName) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';
                }
                else {
                    content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=normal&StrCode=' + stcode + "&Tempid=" + tempid + "&Pname=" + escape(pname) + '&SubId=' + subid + '&SubName=' + escape(_showName) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';
                }

                setTimeout(function () {
                    InfoWindowOnMap.ShowInfoWindowOnMap("站点详情", content, 355, 340, point);
                    InfoWindowOnMap.SetInfowTitleClass();
                    var screenPoint = map.toScreen(point);
                    MovePointPosition(screenPoint);
                    //map.infoWindow.setContent(content);
                    //map.infoWindow.setTitle("");
                    //map.infoWindow.resize(385, 325); //设置infoWindow 窗体的大小
                    //var screenPoint = map.toScreen(point);
                    //map.infoWindow.show(screenPoint, map.getInfoWindowAnchor(screenPoint)); //infowindow在屏幕中的点的位置  ANCHOR_LOWERLEFT
                    //InfoWindowOnMap.SetInfowTitleClass();

                    MarkerSymbolLayer.MarkerSymbolVisibleOnMap();
                    if (lever.value > GraphicMarkImg.st_low) {
                        MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
                    }
                    else {
                        MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
                    }

                }, 800);

                SetLoadingImg("progressBar", "", "", "", "", "hide");
            },
            error: function (data) {
                SetLoadingImg("progressBar", "", "", "", "", "hide");
            }
        });
    },
    SetCompanyListHeight: function () {
        var _heightRight = $("#right").height();
        var _heighBot = $("#bot").height();
        var _listH = parseInt(_heightRight) - parseInt(_heighBot);
        $("#divList").height(_listH);
    }
};

var GraphicMarkImg = {
    st_low: 0,
    st_middle: 2,
    st_high: 2,


    //废水
    img_fs_red_Path: "images/mark/RedPin1LargeB.png",
    img_fs_red_Width: 32,
    img_fs_red_Height: 60,
    img_fs_red_s_Path: "images/mark/RedPin1LargeB_f.gif",
    img_fs_red_s_Width: 32,
    img_fs_red_s_Height: 60,
    img_fs_orange_Path: "images/mark/RedPin1LargeB.png",
    img_fs_orange_Width: 32,
    img_fs_orange_Height: 60,
    img_fs_orange_s_Path: "images/mark/RedPin1LargeB_f.gif",
    img_fs_orange_s_Width: 32,
    img_fs_orange_s_Height: 60,
    img_fs_green_Path: "images/mark/GreenPin1LargeB.png",
    img_fs_green_Width: 32,
    img_fs_green_Height: 60,
    img_fs_green_s_Path: "images/mark/GreenPin1LargeB_f.gif",
    img_fs_green_s_Width: 32,
    img_fs_green_s_Height: 60,

    //废气
    img_fq_red_Path: "images/mark/RedPin1LargeB.png",
    img_fq_red_Width: 32,
    img_fq_red_Height: 60,
    img_fq_red_s_Path: "images/mark/RedPin1LargeB_f.gif",
    img_fq_red_s_Width: 32,
    img_fq_red_s_Height: 60,
    img_fq_orange_Path: "images/mark/RedPin1LargeB.png",
    img_fq_orange_Width: 32,
    img_fq_orange_Height: 60,
    img_fq_orange_s_Path: "images/mark/RedPin1LargeB_f.gif",
    img_fq_orange_s_Width: 32,
    img_fq_orange_s_Height: 60,
    img_fq_green_Path: "images/mark/GreenPin1LargeB.png",
    img_fq_green_Width: 32,
    img_fq_green_Height: 60,
    img_fq_green_s_Path: "images/mark/GreenPin1LargeB_f.gif",
    img_fq_green_s_Width: 32,
    img_fq_green_s_Height: 60,

    //城市污水
    img_wsc_red_Path: "images/mark/RedPin1LargeB.png",
    img_wsc_red_Width: 32,
    img_wsc_red_Height: 60,
    img_wsc_red_s_Path: "images/mark/RedPin1LargeB_f.gif",
    img_wsc_red_s_Width: 32,
    img_wsc_red_s_Height: 60,
    img_wsc_orange_Path: "images/mark/RedPin1LargeB.png",
    img_wsc_orange_Width: 32,
    img_wsc_orange_Height: 60,
    img_wsc_orange_s_Path: "images/mark/RedPin1LargeB_f.gif",
    img_wsc_orange_s_Width: 32,
    img_wsc_orange_s_Height: 60,
    img_wsc_green_Path: "images/mark/GreenPin1LargeB.png",
    img_wsc_green_Width: 32,
    img_wsc_green_Height: 60,
    img_wsc_green_s_Path: "images/mark/GreenPin1LargeB_f.gif",
    img_wsc_green_s_Width: 32,
    img_wsc_green_s_Height: 60,


    //正常图标
    img_Normal: function () {
        var _img = {};
        _img.Path = "images/mark/GreenPin1LargeB.png";
        _img.Width = 32;
        _img.Height = 60;

        var _img_f = {};
        _img_f.Path = "images/mark/GreenPin1LargeB_f.gif";
        _img_f.Width = 40;
        _img_f.Height = 70;

        var _arrayImg = {};
        _arrayImg.Img = _img;
        _arrayImg.Img_f = _img_f;
        return _arrayImg;
    },
    //超标图标
    img_Over: function () {
        var _img = {};
        _img.Path = "images/mark/RedPin1LargeB.png";
        _img.Width = 32;
        _img.Height = 60;

        var _img_f = {};
        _img_f.Path = "images/mark/RedPin1LargeB_f.gif";
        _img_f.Width = 40;
        _img_f.Height = 70;

        var _arrayImg = {};
        _arrayImg.Img = _img;
        _arrayImg.Img_f = _img_f;
        return _arrayImg;
    },
    //停产图标
    img_Stop: function () {
        var _img = {};
        _img.Path = "images/mark/GrayPin1LargeB.png";
        _img.Width = 32;
        _img.Height = 60;

        var _img_f = {};
        _img_f.Path = "images/mark/GrayPin1LargeB_f.gif";
        _img_f.Width = 40;
        _img_f.Height = 70;

        var _arrayImg = {};
        _arrayImg.Img = _img;
        _arrayImg.Img_f = _img_f;
        return _arrayImg;
    },
    //故障图标
    img_Malfunction: function () {
        var _img = {};
        _img.Path = "images/mark/YellowPin1LargeB.png";
        _img.Width = 32;
        _img.Height = 60;

        var _img_f = {};
        _img_f.Path = "images/mark/YellowPin1LargeB_f.gif";
        _img_f.Width = 40;
        _img_f.Height = 70;

        var _arrayImg = {};
        _arrayImg.Img = _img;
        _arrayImg.Img_f = _img_f;
        return _arrayImg;
    },

    //城市图标
    img_citylayer_Path: "images/mark/citylayer.png",
    img_citylayer_Width: 60,
    img_citylayer_Height: 25,


    //城市超标图标
    img_overcitylayer_Path: "images/mark/citylayer_cb.png",
    img_overcitylayer_Width: 60,
    img_overcitylayer_Height: 25,


    GetPictureMarkerSymbol: function (itemType, itemLever) {
        var pictureMarkerSymbol = null;
        switch (itemType) {
            case "WasteWaterGis":
                if (parseFloat(itemLever) == GraphicMarkImg.st_low) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_green_Path, GraphicMarkImg.img_fs_green_Width, GraphicMarkImg.img_fs_green_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_low && parseFloat(itemLever) <= GraphicMarkImg.st_middle) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_orange_Path, GraphicMarkImg.img_fs_orange_Width, GraphicMarkImg.img_fs_orange_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_high) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_red_Path, GraphicMarkImg.img_fs_red_Width, GraphicMarkImg.img_fs_red_Height);
                }
                /*
                switch (itemLever) {
                case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_green_Path, GraphicMarkImg.img_fs_green_Width, GraphicMarkImg.img_fs_green_Height);
                break;
                case "1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_orange_Path, GraphicMarkImg.img_fs_orange_Width, GraphicMarkImg.img_fs_orange_Height);
                break;
                case "2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_red_Path, GraphicMarkImg.img_fs_red_Width, GraphicMarkImg.img_fs_red_Height);
                break;
                }
                */
                break;
            case "WasteAirGis":
                if (parseFloat(itemLever) == GraphicMarkImg.st_low) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_green_Path, GraphicMarkImg.img_fq_green_Width, GraphicMarkImg.img_fq_green_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_low && parseFloat(itemLever) <= GraphicMarkImg.st_middle) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_orange_Path, GraphicMarkImg.img_fq_orange_Width, GraphicMarkImg.img_fq_orange_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_high) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_red_Path, GraphicMarkImg.img_fq_red_Width, GraphicMarkImg.img_fq_red_Height);
                }
                /*
                switch (itemLever) {
                case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_red_Path, GraphicMarkImg.img_fq_red_Width, GraphicMarkImg.img_fq_red_Height);
                break;
                case "1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_orange_Path, GraphicMarkImg.img_fq_orange_Width, GraphicMarkImg.img_fq_orange_Height);
                break;
                case "2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_green_Path, GraphicMarkImg.img_fq_green_Width, GraphicMarkImg.img_fq_green_Height);
                break;
                } */
                break;
            case "SewagePlantGis":
                if (parseFloat(itemLever) == GraphicMarkImg.st_low) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_green_Path, GraphicMarkImg.img_wsc_green_Width, GraphicMarkImg.img_wsc_green_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_low && parseFloat(itemLever) <= GraphicMarkImg.st_middle) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_orange_Path, GraphicMarkImg.img_wsc_orange_Width, GraphicMarkImg.img_wsc_orange_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_high) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_red_Path, GraphicMarkImg.img_wsc_red_Width, GraphicMarkImg.img_wsc_red_Height);
                }
                /*
                switch (itemLever) {
                case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_red_Path, GraphicMarkImg.img_wsc_red_Width, GraphicMarkImg.img_wsc_red_Height);
                break;
                case "1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_orange_Path, GraphicMarkImg.img_wsc_orange_Width, GraphicMarkImg.img_wsc_orange_Height);
                break;
                case "2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_green_Path, GraphicMarkImg.img_wsc_green_Width, GraphicMarkImg.img_wsc_green_Height);
                break;
                }  */
                break;
        }
        return pictureMarkerSymbol;
    },
    GetPictureMarkerSymbol_Falsh: function (itemType, itemLever) {
        var pictureMarkerSymbol = null;
        switch (itemType) {
            case "WasteWaterGis":
                if (parseFloat(itemLever) == GraphicMarkImg.st_low) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_green_s_Path, GraphicMarkImg.img_fs_green_s_Width, GraphicMarkImg.img_fs_green_s_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_low && parseFloat(itemLever) <= GraphicMarkImg.st_middle) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_orange_s_Path, GraphicMarkImg.img_fs_orange_s_Width, GraphicMarkImg.img_fs_orange_s_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_high) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_red_s_Path, GraphicMarkImg.img_fs_red_s_Width, GraphicMarkImg.img_fs_red_s_Height);
                }
                /*
                switch (itemLever) {
                case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_red_s_Path, GraphicMarkImg.img_fs_red_s_Width, GraphicMarkImg.img_fs_red_s_Height);
                break;
                case "1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_orange_s_Path, GraphicMarkImg.img_fs_orange_s_Width, GraphicMarkImg.img_fs_orange_s_Height);
                break;
                case "2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fs_green_s_Path, GraphicMarkImg.img_fs_green_s_Width, GraphicMarkImg.img_fs_green_s_Height);
                break;
                }  */
                break;
            case "WasteAirGis":
                if (parseFloat(itemLever) == GraphicMarkImg.st_low) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_green_s_Path, GraphicMarkImg.img_fq_green_s_Width, GraphicMarkImg.img_fq_green_s_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_low && parseFloat(itemLever) <= GraphicMarkImg.st_middle) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_orange_s_Path, GraphicMarkImg.img_fq_orange_s_Width, GraphicMarkImg.img_fq_orange_s_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_high) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_red_s_Path, GraphicMarkImg.img_fq_red_s_Width, GraphicMarkImg.img_fq_red_s_Height);
                }
                /*
                switch (itemLever) {
                case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_red_s_Path, GraphicMarkImg.img_fq_red_s_Width, GraphicMarkImg.img_fq_red_s_Height);
                break;
                case "1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_orange_s_Path, GraphicMarkImg.img_fq_orange_s_Width, GraphicMarkImg.img_fq_orange_s_Height);
                break;
                case "2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_fq_green_s_Path, GraphicMarkImg.img_fq_green_s_Width, GraphicMarkImg.img_fq_green_s_Height);
                break;
                }
                */
                break;
            case "SewagePlantGis":
                if (parseFloat(itemLever) == GraphicMarkImg.st_low) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_green_s_Path, GraphicMarkImg.img_wsc_green_s_Width, GraphicMarkImg.img_wsc_green_s_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_low && parseFloat(itemLever) <= GraphicMarkImg.st_middle) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_orange_s_Path, GraphicMarkImg.img_wsc_orange_s_Width, GraphicMarkImg.img_wsc_orange_s_Height);
                }
                if (parseFloat(itemLever) > GraphicMarkImg.st_high) {
                    pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_red_s_Path, GraphicMarkImg.img_wsc_red_s_Width, GraphicMarkImg.img_wsc_red_s_Height);
                }
                /*
                switch (itemLever) {
                case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_red_s_Path, GraphicMarkImg.img_wsc_red_s_Width, GraphicMarkImg.img_wsc_red_s_Height);
                break;
                case "1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_orange_s_Path, GraphicMarkImg.img_wsc_orange_s_Width, GraphicMarkImg.img_wsc_orange_s_Height);
                break;
                case "2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_wsc_green_s_Path, GraphicMarkImg.img_wsc_green_s_Width, GraphicMarkImg.img_wsc_green_s_Height);
                break;
                }
                */
                break;
        }
        return pictureMarkerSymbol;
    },
    //Add by mercy@20150420am 进行调整，不再区分废水、废气、污水，统一图标
    GetPictureMarkerSymbol_New: function (itemLever) {
        var pictureMarkerSymbol = null;
        /*
        * 站点异常-5
        * 正常-0
        * 超标-显示超标的值
        * 停产- -1
        * 故障- -2 
        */
        switch (itemLever) {
            case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Normal().Img.Path, GraphicMarkImg.img_Normal().Img.Width, GraphicMarkImg.img_Normal().Img.Height);
                break;
            case "-1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Stop().Img.Path, GraphicMarkImg.img_Stop().Img.Width, GraphicMarkImg.img_Stop().Img.Height);
                break;
            case "-2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Malfunction().Img.Path, GraphicMarkImg.img_Malfunction().Img.Width, GraphicMarkImg.img_Malfunction().Img.Height);
                break;
            case "-5":
                //程序异常的情况
                break;
            default:
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Over().Img.Path, GraphicMarkImg.img_Over().Img.Width, GraphicMarkImg.img_Over().Img.Height);
                break;
        }
        return pictureMarkerSymbol;
    },
    //Add by mercy@20150420am 进行调整，不再区分废水、废气、污水，统一图标
    GetPictureMarkerSymbol_Falsh_New: function (itemLever) {
        var pictureMarkerSymbol = null;
        /*
        * 站点异常-5
        * 正常-0
        * 超标-显示超标的值
        * 停产- -1
        * 故障- -2 
        */
        switch (itemLever) {
            case "0":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Normal().Img_f.Path, GraphicMarkImg.img_Normal().Img_f.Width, GraphicMarkImg.img_Normal().Img_f.Height);
                break;
            case "-1":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Stop().Img_f.Path, GraphicMarkImg.img_Stop().Img_f.Width, GraphicMarkImg.img_Stop().Img_f.Height);
                break;
            case "-2":
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Malfunction().Img_f.Path, GraphicMarkImg.img_Malfunction().Img_f.Width, GraphicMarkImg.img_Malfunction().Img_f.Height);
                break;
            case "-5":
                //程序异常的情况
                break;
            default:
                pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_Over().Img_f.Path, GraphicMarkImg.img_Over().Img_f.Width, GraphicMarkImg.img_Over().Img_f.Height);
                break;
        }
        return pictureMarkerSymbol;
    }
};

var CityLayer = {
    ZoomLever: 8,
    SetStationCountLayer: function (cityList) {

        var Wfeaturelayer = map.getLayer("MarkeSymbol_CityLayer"); //获取到图层
        if (Wfeaturelayer) {
            map.removeLayer(Wfeaturelayer); //移除当前图层
            //Wfeaturelayer.clear();
        }


        var graphicLayer_CityLayer = new esri.layers.GraphicsLayer({ id: "MarkeSymbol_CityLayer" }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        map.addLayers([graphicLayer_CityLayer]);

        var _json = eval("(" + cityList + ")");
        dojo.forEach(_json.CityList, function (item, index) {
            //var symbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 15,
            //             new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,
            //             new esri.Color([95, 150, 215, 0.8]), 15),
            //             new esri.Color([95, 150, 215, 0.8]));  // map.spatialReference({ wkid: 4326 })

            var symbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_citylayer_Path, GraphicMarkImg.img_citylayer_Width, GraphicMarkImg.img_citylayer_Height);
            var symbol_over = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_overcitylayer_Path, GraphicMarkImg.img_overcitylayer_Width, GraphicMarkImg.img_overcitylayer_Height);

            if (item.Count) {
                var point = new esri.geometry.Point(item.Longitude, item.Latitude, map.spatialReference);

                var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
                var textSymbol = new esri.symbol.TextSymbol(item.OverCount + "/" + item.Count, font, new esri.Color([255, 255, 255, 1])).setOffset(0, -5);

                //var _graphicSymbol = new esri.Graphic(pointMeters, symbol);
                //Add by mercy@20150418 添加超标红色背景图片
                var _graphicSymbol = null; // new esri.Graphic(pointMeters, symbol);
                if (item.OverCount > 0) {
                    _graphicSymbol = new esri.Graphic(point, symbol_over);
                }
                else {
                    _graphicSymbol = new esri.Graphic(point, symbol);
                }


                var _graphicText = new esri.Graphic(point, textSymbol);
                var attr = {};
                attr["subid"] = item.cityCode + "_" + index;
                attr["citycode"] = item.cityCode; //描述内容
                attr["Longitude"] = item.Longitude; //描述内容
                attr["Latitude"] = item.Latitude; //描述内容
                attr["Count"] = item.Count; //数量

                _graphicText.setAttributes(attr); //设置画布属性
                _graphicSymbol.setAttributes(attr); //设置画布属性

                graphicLayer_CityLayer.add(_graphicSymbol);
                graphicLayer_CityLayer.add(_graphicText);
            }
        });
        //添加事件处理
        graphicLayer_CityLayer.on("click", function (evt) {
            var _cityCode = evt.graphic.attributes.citycode;
            var _Longitude = evt.graphic.attributes.Longitude;
            var _Latitude = evt.graphic.attributes.Latitude;

            //在切换项目时城市列表被选中
            OnClickCityHandler("li_" + _cityCode, "", _cityCode, _Longitude, _Latitude); //OnClickCityHandler(objid, cityName, cityCode, longitude, latitude)

            // CityLayer.ClearCityLayer("MarkeSymbol_CityLayer");
            //CityLayer.FlyToLocation(_Longitude, _Latitude, 5);
        });

        graphicLayer_CityLayer.on("mouse-over", function (evt) {
            map.setMapCursor("pointer");
            return;

            var subid = evt.graphic.attributes.subid;

            var l = map.getLayer("MarkeSymbol_CityLayer"); //根据FeatureLayer的ID获取到该图层
            var graphicS = l.graphics; //获取当前图层内所有的graphic画板.可以循环这个对象 查找到要找的symbol
            var symbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 20,
                new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                    new esri.Color([95, 150, 215, 0.8]), 20),
                new esri.Color([95, 150, 215, 0.8]));  // map.spatialReference({ wkid: 4326 })
            var symbolHigh = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 20,
                new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                    new esri.Color([255, 150, 215, 0.2]), 20),
                new esri.Color([95, 150, 215, 1]));  // map.spatialReference({ wkid: 4326 })

            for (var i = 0; i < graphicS.length; i++) {
                var subidAttr = graphicS[i].attributes.subid;
                if (subidAttr == subid) {

                    var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
                    var textSymbol = new esri.symbol.TextSymbol(item.Count, font, new esri.Color([255, 255, 255, 1])).setOffset(0, -8);
                    var _graphicText = new esri.Graphic(evt.graphic.geometry, textSymbol);

                    graphicS[i].setSymbol(symbolHigh);
                    //graphicS[i].setSymbol(_graphicText);

                    //map.infoWindow.setFeatures([graphicS[i]]);//infoWindow的选中形式
                }
                else {
                    //graphicS[i].setSymbol(symbol);
                }
            }

        });
        graphicLayer_CityLayer.on("mouse-out", function (evt) {
            map.setMapCursor("default");
        });

        MarkerSymbolLayer.MarkerSymbolVisibleOnMap();
    },
    SetStationCountLayer_New: function (cityList) {

        var Wfeaturelayer = map.getLayer("MarkeSymbol_CityLayer"); //获取到图层
        if (Wfeaturelayer) {
            map.removeLayer(Wfeaturelayer); //移除当前图层
        }

        var graphicLayer_CityLayer = new esri.layers.GraphicsLayer({ id: "MarkeSymbol_CityLayer" }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        map.addLayers([graphicLayer_CityLayer]);

        var _json = cityList; //eval("(" + cityList + ")");
		
		//将莱芜市的数据加到济南市中，同时将莱芜市数据删除
        for (var i = 0; i < _json.CityList.length; i++) {
            var json = _json.CityList[i];
            if (json.cityCode == "371200")
            {
                _json.CityList[0].Count += json.Count;

                for (var k = 0; k < json.ItemsInfos; k++) {
                    var item = json.ItemsInfos[k];
                    var obj = new Object();
                    obj.value = item.value;
                    obj.pollItemName = item.pollItemValue;
                    obj.pollItemValue = item.pollItemValue;

                    _json.CityList[0].ItemsInfos.push(obj);
                }

                _json.CityList.splice(i, 1);
                break;
            }
        }
		
        dojo.forEach(_json.CityList, function (item, index) {
            var symbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_citylayer_Path, GraphicMarkImg.img_citylayer_Width, GraphicMarkImg.img_citylayer_Height);
            var symbol_over = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_overcitylayer_Path, GraphicMarkImg.img_overcitylayer_Width, GraphicMarkImg.img_overcitylayer_Height);

            if (item.Count) {

                //Add by mercy@20150421am
                var _arrayInfo = StatisticsEnterpriseTool.GetStatisticsEnterprise(item.ItemsInfos, 1);


                var point = new esri.geometry.Point(item.Longitude, item.Latitude, map.spatialReference);

                var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
                var textSymbol = new esri.symbol.TextSymbol(_arrayInfo.OverCount + "/" + item.Count, font, new esri.Color([255, 255, 255, 1])).setOffset(0, -5);


                //var _graphicSymbol = new esri.Graphic(pointMeters, symbol);
                //Add by mercy@20150418 添加超标红色背景图片
                var _graphicSymbol = null; // new esri.Graphic(pointMeters, symbol);
                if (_arrayInfo.OverCount > 0) {
                    _graphicSymbol = new esri.Graphic(point, symbol_over);
                }
                else {
                    _graphicSymbol = new esri.Graphic(point, symbol);
                }

                var _graphicText = new esri.Graphic(point, textSymbol);
                var attr = {};
                attr["subid"] = item.cityCode + "_" + index;
                attr["citycode"] = item.cityCode; //描述内容
                attr["Longitude"] = item.Longitude; //描述内容
                attr["Latitude"] = item.Latitude; //描述内容
                attr["Count"] = item.Count; //数量

                _graphicText.setAttributes(attr); //设置画布属性
                _graphicSymbol.setAttributes(attr); //设置画布属性

                graphicLayer_CityLayer.add(_graphicSymbol);
                graphicLayer_CityLayer.add(_graphicText);
            }
        });
        //添加事件处理
        graphicLayer_CityLayer.on("click", function (evt) {
            var _cityCode = evt.graphic.attributes.citycode;
            var _Longitude = evt.graphic.attributes.Longitude;
            var _Latitude = evt.graphic.attributes.Latitude;

            //在切换项目时城市列表被选中
            OnClickCityHandler("li_" + _cityCode, "", _cityCode, _Longitude, _Latitude); //OnClickCityHandler(objid, cityName, cityCode, longitude, latitude)
        });

        graphicLayer_CityLayer.on("mouse-over", function (evt) {
            map.setMapCursor("pointer");
            return;
        });
        graphicLayer_CityLayer.on("mouse-out", function (evt) {
            map.setMapCursor("default");
        });

        MarkerSymbolLayer.MarkerSymbolVisibleOnMap();
    },
    //查询数据后地图上生成站点
    ShowStationOnMap: function (stationList) {
        if (stationList == null || stationList.length <= 0) {
            return;
        }
        var graphicLayer_CityLayer = map.getLayer("MarkeSymbol_CityLayer");
        if (graphicLayer_CityLayer == null) {
            graphicLayer_CityLayer = new esri.layers.GraphicsLayer({ id: "MarkeSymbol_CityLayer" });
            map.addLayers([graphicLayer_CityLayer]);
        }
        var symbol = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_citylayer_Path, GraphicMarkImg.img_citylayer_Width, GraphicMarkImg.img_citylayer_Height);
        var symbol_over = new esri.symbol.PictureMarkerSymbol(GraphicMarkImg.img_overcitylayer_Path, GraphicMarkImg.img_overcitylayer_Width, GraphicMarkImg.img_overcitylayer_Height);
        var item, _arrayInfo, point, pointMeters, font, textSymbol;
        for (var i = 0; i < stationList.length; i++) {
            item = stationList[i], _arrayInfo = StatisticsEnterpriseTool.GetStatisticsEnterprise(item.ItemsInfos, 1);
            point = new esri.geometry.Point(item.Longitude, item.Latitude, map.spatialReference);
            

            font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            textSymbol = new esri.symbol.TextSymbol(_arrayInfo.OverCount + "/" + item.Count, font, new esri.Color([255, 255, 255, 1])).setOffset(0, -5);

            var _graphicSymbol = null; // new esri.Graphic(pointMeters, symbol);
            if (_arrayInfo.OverCount > 0) {
                _graphicSymbol = new esri.Graphic(point, symbol_over);
            }
            else {
                _graphicSymbol = new esri.Graphic(point, symbol);
            }

            var _graphicText = new esri.Graphic(point, textSymbol);
            var attr = {};
            attr["subid"] = item.cityCode + "_" + index;
            attr["citycode"] = item.cityCode; //描述内容
            attr["Longitude"] = item.Longitude; //描述内容
            attr["Latitude"] = item.Latitude; //描述内容
            attr["Count"] = item.Count; //数量

            _graphicText.setAttributes(attr); //设置画布属性
            _graphicSymbol.setAttributes(attr); //设置画布属性

            graphicLayer_CityLayer.add(_graphicSymbol);
            graphicLayer_CityLayer.add(_graphicText);
        }
    },
    FlyToLocation: function (Longitude, Latitude, lever) {
        var point = new esri.geometry.Point([parseFloat(Longitude), parseFloat(Latitude)]); //, map.spatialReference);
        map.centerAndZoom(point, lever);
    },
    ClearCityLayer: function (layerName) {
        var _array = layerName.split(',');
        for (var i = 0; i < _array.length; i++) {
            if (_array[0] == '') return;
            var Wfeaturelayer = map.getLayer(_array[i]); //获取到图层
            if (Wfeaturelayer) {
                map.removeLayer(Wfeaturelayer); //移除当前图层
            }
        }
    }
};

var MarkerSymbolLayer = {
    graphic_Text: "_Text",
    MarkerSymbol: "MarkerSymbol",
    MarkerSymbol_Pic_High: "MarkerSymbol_Pic_High",
    MarkerSymbol_Pic_High_Text: "MarkerSymbol_Pic_High_Text",
    MarkerSymbol_Pic_Low: "MarkerSymbol_Pic_Low",
    MarkerSymbol_Pic_Low_Text: "MarkerSymbol_Pic_Low_Text",

    AddMarkerSymbol: function (stcode, type) {
        InfoWindowOnMap.CloseInfoWindow(); //关闭infowindow窗体
        //var requestHandle = esri.request({
        //    url: "ajax/map.ashx?Method=SelectSubList&stcode=" + stcode + "&type=" + type,
        //    handleAs: "json"
        //});
        //requestHandle.then(
        //                function (response) {
        //                    MarkerSymbolLayer.RequestSucceeded_graphic(response);
        //                },
        //                function (error) {
        //                    MarkerSymbolLayer.RequestFailed(error);
        //                });
		
		if(stcode=="370100")
		{
			stcode+=",371200";
		}
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: "ajax/map.ashx",
            data: {
                Method: 'SelectSubList',
                stcode: stcode,
                type: type,
                isall: !MapSetting.hasOwnProperty("isAll") ? "0" : MapSetting.isAll
            },
            success: function (data) {
                if (data == "") return;
                //var jsonData = eval(data);
                MarkerSymbolLayer.RequestSucceeded_graphic(data, stcode);
            },
            error: function (error) {
                MarkerSymbolLayer.RequestFailed(error);
            },
            complete: function (XMLHttpRequest, textStatus) {
                //HideLoading();
            }
        });
    },
    RequestSucceeded_Query_clear: function () {
        //如果图层存在先移除
        var Wfeaturelayer = map.getLayer(MarkerSymbolLayer.MarkerSymbol); //获取到图层
        var Wfeaturelayer_Pic_High = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High); //获取到图层
        var Wfeaturelayer_Pic_High_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High_Text); //获取到图层
        var Wfeaturelayer_Pic_Low = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low); //获取到图层
        var Wfeaturelayer_Pic_Low_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text); //获取到图层

        var HightBorderSymbolLayer = map.getLayer("HightBorderSymbol"), cityLayer = map.getLayer("MarkeSymbol_CityLayer");

        if (HightBorderSymbolLayer) {
            HightBorderSymbolLayer.clear();
        }
        if (cityLayer) {
            cityLayer.clear();
        }
        if (Wfeaturelayer) {
            map.removeLayer(Wfeaturelayer); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High) {
            map.removeLayer(Wfeaturelayer_Pic_High); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low) {
            map.removeLayer(Wfeaturelayer_Pic_Low); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High_Text) {
            map.removeLayer(Wfeaturelayer_Pic_High_Text); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low_Text) {
            map.removeLayer(Wfeaturelayer_Pic_Low_Text); //移除当前图层
        }


    },
    RequestSucceeded_Query_graphic: function (items) {
        //如果图层存在先移除
        var Wfeaturelayer = map.getLayer(MarkerSymbolLayer.MarkerSymbol); //获取到图层
        var Wfeaturelayer_Pic_High = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High); //获取到图层
        var Wfeaturelayer_Pic_High_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High_Text); //获取到图层
        var Wfeaturelayer_Pic_Low = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low); //获取到图层
        var Wfeaturelayer_Pic_Low_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text); //获取到图层

        var HightBorderSymbolLayer = map.getLayer("HightBorderSymbol"), cityLayer = map.getLayer("MarkeSymbol_CityLayer");

        if (HightBorderSymbolLayer) {
            HightBorderSymbolLayer.clear();
        }
        if (cityLayer) {
            cityLayer.clear();
        }
        if (Wfeaturelayer) {
            map.removeLayer(Wfeaturelayer); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High) {
            map.removeLayer(Wfeaturelayer_Pic_High); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low) {
            map.removeLayer(Wfeaturelayer_Pic_Low); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High_Text) {
            map.removeLayer(Wfeaturelayer_Pic_High_Text); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low_Text) {
            map.removeLayer(Wfeaturelayer_Pic_Low_Text); //移除当前图层
        }
        var graphicLayer = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_High = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_High, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_Low = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_Low, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_High_Text = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_High_Text, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_Low_Text = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol


        map.addLayers([graphicLayer, graphicLayer_Pic_High, graphicLayer_Pic_Low, graphicLayer_Pic_High_Text, graphicLayer_Pic_Low_Text]);

        if (map.infoWindow.isShowing) {
            map.infoWindow.hide();
        }

        var features = [], multipoint = new esri.geometry.Multipoint(new esri.SpatialReference({ wkid: 102113 }));
        if (items != null && items.length > 0) {
            dojo.forEach(items, function (item) {
                var attr = {};
                attr["pname"] = item.pname; //描述内容
                attr["areaname"] = item.areaname; //所在的区名
                attr["subid"] = item.subid ? item.subid : "没有查询到数据"; //标题,站点名称
                attr["stcode"] = item.stcode; //行政区划编号
                attr["type"] = item.type;
                attr["lever"] = item.lever; //是否超标
                attr["enterpriseCode"] = item.enterpriseCode; //企业编号
                attr["tempid"] = item.tempid;

                var attr_text = {};
                attr_text["pname"] = item.pname; //企业名称
                attr_text["areaname"] = item.areaname; //所在的区名
                attr_text["subid"] = item.subid + "_Text"; //站点编号
                attr_text["stcode"] = item.stcode; //行政区划编号
                attr_text["type"] = item.type;
                attr_text["lever"] = item.lever;
                attr_text["tempid"] = item.tempid;


                var point = new esri.geometry.Point(parseFloat(item.longitude), parseFloat(item.latitude)); //创建点
                multipoint.addPoint(point);

                var pictureMarkerSymbol = GraphicMarkImg.GetPictureMarkerSymbol_New(item.lever.value);

                var graphic = new esri.Graphic(point, pictureMarkerSymbol); //画布
                graphic.setAttributes(attr); //设置画布属性

                graphicLayer.add(graphic);

                var font = new esri.symbol.Font().setSize("8pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);

                //对超标倍数高的在地图上显示
                if (item.lever.value > GraphicMarkImg.st_low) {
                    var textSymbol = new esri.symbol.TextSymbol(Math.round(item.lever.pollItemValue), font, new esri.Color([255, 255, 255, 0.8])).setOffset(0, 9);
                    
                    var graphic_text = new esri.Graphic(point, textSymbol);
                    graphic_text.setAttributes(attr_text); //设置画布属性

                    graphicLayer_Pic_High_Text.add(graphic_text);
                    graphicLayer_Pic_High.add(graphic);
                }
                else {
                    var textSymbol = new esri.symbol.TextSymbol(Math.round(item.lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(0, -8);
                    
                    var graphic_text = new esri.Graphic(point, textSymbol);
                    graphic_text.setAttributes(attr_text); //设置画布属性
                    graphicLayer.add(graphic_text);
                    graphicLayer_Pic_Low.add(graphic);
                }
            });
        }


        graphicLayer_Pic_Low.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var areaname = evt.graphic.attributes.areaname; //所在的区名
            var subid = evt.graphic.attributes.subid;
            //var stcode = evt.graphic.attributes.stcode;
            //var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever;

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + "--" + areaname, font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));


            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_High.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var lever = evt.graphic.attributes.lever;

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + ", " + lever.pollItemName + "，" + Math.round(lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_High_Text.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var lever = evt.graphic.attributes.lever;

            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + ", " + lever.pollItemName + "，" + Math.round(lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_Low.on("click", function (evt) {

            //var screenPoint = MovePointPosition_Click(evt.screenPoint);

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;


            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=normal&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';

            //var content = "hello world";

            setTimeout(function () {
                InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });
        graphicLayer_Pic_High.on("click", function (evt) {
            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=high&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';


            setTimeout(function () {
                InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });
        graphicLayer_Pic_High_Text.on("click", function (evt) {
            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=high&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';


            setTimeout(function () {
                //map.infoWindow.setContent(content);
                //map.infoWindow.setTitle("站点详情");
                //map.infoWindow.resize(390, 340); //设置infoWindow 窗体的大小
                //map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置
                //InfoWindowOnMap.SetInfowTitleClass();

                InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_Low.on("mouse-out", function (evt) {

            map.graphics.clear();
            map.setMapCursor("default");

        });
        graphicLayer_Pic_High.on("mouse-out", function (evt) {

            map.graphics.clear();
            map.setMapCursor("default");

        });

        //设置图标的显示与隐藏
        MarkerSymbolLayer.MarkerSymbolVisibleOnMap();

        if (multipoint.points.length > 0) {
            var multipointExtent = multipoint.getExtent();
            if (multipointExtent == null
                || isNaN(multipointExtent.xmax)
                || isNaN(multipointExtent.xmax)
                || isNaN(multipointExtent.xmax)
                || isNaN(multipointExtent.xmax)) {
                map.centerAndZoom(new esri.geometry.Point(118.00185147981321, 36.46908695152021), 4);
            } else {
                map.setExtent(multipointExtent);
            }

        }
    },
    
    RequestSucceeded_graphic: function (response, objcode) {
        //如果图层存在先移除
        var Wfeaturelayer = map.getLayer(MarkerSymbolLayer.MarkerSymbol); //获取到图层
        var Wfeaturelayer_Pic_High = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High); //获取到图层
        var Wfeaturelayer_Pic_High_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High_Text); //获取到图层
        var Wfeaturelayer_Pic_Low = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low); //获取到图层
        var Wfeaturelayer_Pic_Low_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text); //获取到图层

        if (Wfeaturelayer) {
            map.removeLayer(Wfeaturelayer); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High) {
            map.removeLayer(Wfeaturelayer_Pic_High); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low) {
            map.removeLayer(Wfeaturelayer_Pic_Low); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High_Text) {
            map.removeLayer(Wfeaturelayer_Pic_High_Text); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low_Text) {
            map.removeLayer(Wfeaturelayer_Pic_Low_Text); //移除当前图层
        }

        //, minScale: 577790.55 
        //opacity=0 Known values: 0.0 - 1.0 Default value: 1.0
        var graphicLayer = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_High = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_High, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_Low = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_Low, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_High_Text = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_High_Text, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_Low_Text = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol


        map.addLayers([graphicLayer, graphicLayer_Pic_High, graphicLayer_Pic_Low, graphicLayer_Pic_High_Text, graphicLayer_Pic_Low_Text]);

        //map.addLayers([graphicLayer_Pic_High]);
        //map.addLayers([graphicLayer_Pic_Low]);
        //map.addLayers([graphicLayer_Pic_High_Text]);
        //map.addLayers([graphicLayer_Pic_Low_Text]);

        //loop through the items and add to the graphic layer遍历返回的json的items对象,并添加到graphic layer
        var features = [];
        dojo.forEach(response.items, function (item) {
            var attr = {};
            attr["pname"] = item.pname; //描述内容
            attr["areaname"] = item.areaname; //所在的区名
            attr["subid"] = item.subid ? item.subid : "没有查询到数据"; //标题,站点名称
            attr["stcode"] = item.stcode; //行政区划编号
            attr["type"] = item.type;
            attr["lever"] = item.lever; //是否超标
            attr["enterpriseCode"] = item.enterpriseCode; //企业编号
            attr["tempid"] = item.tempid;
            var attr_text = {};
            attr_text["pname"] = item.pname; //企业名称
            attr_text["areaname"] = item.areaname; //所在的区名
            attr_text["subid"] = item.subid + "_Text"; //站点编号
            attr_text["stcode"] = item.stcode; //行政区划编号
            attr_text["type"] = item.type;
            attr_text["lever"] = item.lever;
            attr_text["tempid"] = item.tempid;




            var point = new esri.geometry.Point(parseFloat(item.longitude), parseFloat(item.latitude)); //创建点
            //var pictureMarkerSymbol = GraphicMarkImg.GetPictureMarkerSymbol(item.type, item.lever.value); // new esri.symbol.PictureMarkerSymbol(imgPath, imgWidth, imgHeight); //！！！pwat.gif
            var pictureMarkerSymbol = GraphicMarkImg.GetPictureMarkerSymbol_New(item.lever.value);

            var graphic = new esri.Graphic(point, pictureMarkerSymbol); //画布
            graphic.setAttributes(attr); //设置画布属性

            graphicLayer.add(graphic);

            var font = new esri.symbol.Font().setSize("8pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);

            //对超标倍数高的在地图上显示
            if (item.lever.value > GraphicMarkImg.st_low) {
                var textSymbol = new esri.symbol.TextSymbol(Math.round(item.lever.pollItemValue), font, new esri.Color([255, 255, 255, 0.8])).setOffset(0, 9);
                
                var graphic_text = new esri.Graphic(point, textSymbol);
                graphic_text.setAttributes(attr_text); //设置画布属性

                //graphicLayer.add(graphic_text);

                //graphicLayer_Pic_High_Text.add(graphic_text);

                //graphicLayer_Pic_Low.add(graphic);
                graphicLayer_Pic_High_Text.add(graphic_text);
                graphicLayer_Pic_High.add(graphic);
            }
            else {
                var textSymbol = new esri.symbol.TextSymbol(Math.round(item.lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(0, -8);
                
                var graphic_text = new esri.Graphic(point, textSymbol);
                graphic_text.setAttributes(attr_text); //设置画布属性
                graphicLayer.add(graphic_text);
                //
                //graphicLayer_Pic_Low_Text.add(graphic_text);
                graphicLayer_Pic_Low.add(graphic);
            }
        });

        graphicLayer_Pic_Low.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var areaname = evt.graphic.attributes.areaname; //所在的区名
            var subid = evt.graphic.attributes.subid;
            //var stcode = evt.graphic.attributes.stcode;
            //var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever;

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + "--" + areaname, font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));

            // var MarkerSymbol_High = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High); //根据FeatureLayer的ID获取到该图层
            // var MarkerSymbol_Low = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low); //根据FeatureLayer的ID获取到该图层
            //
            // map.reorderLayer(MarkerSymbol_Low, 13);  //调整图层的顺序
            // map.reorderLayer(MarkerSymbol_High, 15);

            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);


            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_High.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            //var stcode = evt.graphic.attributes.stcode;
            //var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever;

            //var r = Math.floor(Math.random() * 250);
            //var g = Math.floor(Math.random() * 100);
            //var b = Math.floor(Math.random() * 100);
            //var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            //var textSymbol = new esri.symbol.TextSymbol(pname + ": " + subid, font, new esri.Color([r, g, b, 0.8])).setOffset(5, 15);
            //

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + ", " + lever.pollItemName + "，" + Math.round(lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_High_Text.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            //var stcode = evt.graphic.attributes.stcode;
            //var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever;

            //var r = Math.floor(Math.random() * 250);
            //var g = Math.floor(Math.random() * 100);
            //var b = Math.floor(Math.random() * 100);
            //var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            //var textSymbol = new esri.symbol.TextSymbol(pname + ": " + subid, font, new esri.Color([r, g, b, 0.8])).setOffset(5, 15);
            //

            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + ", " + lever.pollItemName + "，" + Math.round(lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_Low.on("click", function (evt) {

            //var screenPoint = MovePointPosition_Click(evt.screenPoint);

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            /*
            * 根据站点编号或企业编号和tree实现选择联动
            * add by liuj@20151230 14:25
            ---------------------开始---------------------*/
            var enterpriseCode = evt.graphic.attributes.enterpriseCode; //企业编号 add by liuj@20151230 10:36
            var selectNode = $('#menutree').tree('find', subid);
            if (!selectNode && enterpriseCode !== subid) {
                //说明显示的标记是某个企业的站点信息，而且 在tree里面 此企业 没有展开,需要根据企业编号获取节点信息
                TreeMenu.isTreeClick = false;
                var enterpriseNode = $('#menutree').tree('find', enterpriseCode);
                if (enterpriseNode != null && enterpriseNode.target != null) {

                    $('#menutree').tree('expand', enterpriseNode.target);
                    $("#menutree").tree('select', enterpriseNode.target); //设置当前节点为选中状态
                }

                TreeMenu.isTreeClick = true;
            }
            if (selectNode && selectNode.attributes.type === 'station') {
                TreeMenu.isTreeClick = false;
                //选择的是站点，说明还有企业父节点信息,需要先获取企业节点，展开，设置当前站点为选中状态
                var parentNode = $('#menutree').tree('getParent', selectNode.target);

                $('#menutree').tree('expand', parentNode.target); //展开父节点
                if (TreeMenu.PreExpandNode && TreeMenu.PreExpandNode.id != parentNode.id) {
                    $('#menutree').tree('collapse', TreeMenu.PreExpandNode.target);
                }
                TreeMenu.PreExpandNode = parentNode;
                $("#menutree").tree('select', selectNode.target); //设置当前节点为选中状态
                TreeMenu.isTreeClick = true;
            }
            if (selectNode && selectNode.attributes.type === 'enterprise') {
                TreeMenu.isTreeClick = false;
                //选择的是企业站点，增加选中状态就行了
                $("#menutree").tree('select', selectNode.target); //设置当前节点为选中状态

                //树上企业高亮显示
                $(".stationNode").each(function (index, item) {
                    $(item).removeClass("stationNode");
                });
                $(selectNode.target).addClass("stationNode");


                if (TreeMenu.PreExpandNode) {
                    $('#menutree').tree('collapse', TreeMenu.PreExpandNode.target);
                }
                TreeMenu.isTreeClick = true;
            }
            /*------------------结束----------------------*/
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=normal&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';

            //var content = "hello world";

            setTimeout(function () {
                //map.infoWindow.setContent(content);
                //map.infoWindow.setTitle("站点详情");
                //map.infoWindow.resize(390, 340); //设置infoWindow 窗体的大小
                //map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置  ANCHOR_LOWERLEFT
                //InfoWindowOnMap.SetInfowTitleClass();

                //InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.ShowInfoWindowOnMapCenter("", content, 355, 340, evt);


                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });
        graphicLayer_Pic_High.on("click", function (evt) {
            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            /*
            * 根据站点编号或企业编号和tree实现选择联动
            * add by liuj@20151230 14:25
            ---------------------开始---------------------*/
            var enterpriseCode = evt.graphic.attributes.enterpriseCode; //企业编号 add by liuj@20151230 10:36
            var selectNode = $('#menutree').tree('find', subid);
            if (!selectNode && enterpriseCode !== subid) {
                //说明显示的标记是某个企业的站点信息，而且 在tree里面 此企业 没有展开,需要根据企业编号获取节点信息
                TreeMenu.isTreeClick = false;
                var enterpriseNode = $('#menutree').tree('find', enterpriseCode);
                $("#menutree").tree('select', enterpriseNode.target); //设置当前节点为选中状态
                $('#menutree').tree('expand', enterpriseNode.target);
                TreeMenu.isTreeClick = true;
            }
            if (selectNode && selectNode.attributes.type === 'station') {
                TreeMenu.isTreeClick = false;
                //选择的是站点，说明还有企业父节点信息,需要先获取企业节点，展开，设置当前站点为选中状态
                var parentNode = $('#menutree').tree('getParent', selectNode.target);
                $('#menutree').tree('expand', parentNode.target); //展开父节点
                $("#menutree").tree('select', selectNode.target); //设置当前节点为选中状态
                TreeMenu.isTreeClick = true;
            }
            if (selectNode && selectNode.attributes.type === 'enterprise') {
                TreeMenu.isTreeClick = false;
                //选择的是企业站点，增加选中状态就行了
                $("#menutree").tree('select', selectNode.target); //设置当前节点为选中状态

                //树上企业高亮显示
                $(".stationNode").each(function (index, item) {
                    $(item).removeClass("stationNode");
                });
                $(selectNode.target).addClass("stationNode");

                TreeMenu.isTreeClick = true;
            }
            /*------------------结束----------------------*/
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=high&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';


            setTimeout(function () {
                //map.infoWindow.setContent(content);
                //map.infoWindow.setTitle("站点详情");
                //map.infoWindow.resize(390, 340); //设置infoWindow 窗体的大小
                //map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置
                //InfoWindowOnMap.SetInfowTitleClass();

                //InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);

                InfoWindowOnMap.ShowInfoWindowOnMapCenter("", content, 355, 340, evt);

                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });
        graphicLayer_Pic_High_Text.on("click", function (evt) {
            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=high&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';


            setTimeout(function () {
                //map.infoWindow.setContent(content);
                //map.infoWindow.setTitle("站点详情");
                //map.infoWindow.resize(390, 340); //设置infoWindow 窗体的大小
                //map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置
                //InfoWindowOnMap.SetInfowTitleClass();

                InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_Low.on("mouse-out", function (evt) {

            map.graphics.clear();
            map.setMapCursor("default");

        });
        graphicLayer_Pic_High.on("mouse-out", function (evt) {

            map.graphics.clear();
            map.setMapCursor("default");

        });

        //设置图标的显示与隐藏
        MarkerSymbolLayer.MarkerSymbolVisibleOnMap();

        //Add by mercy@20150418pm 进行停产、故障、正常、超标个数统计
        //StatisticsEnterpriseTool.StatisticsEnterpriseCount(response.items, objcode);
    },
    
    RequestSucceeded_Query_graphic1: function (items) {
        //如果图层存在先移除
        var Wfeaturelayer = map.getLayer(MarkerSymbolLayer.MarkerSymbol); //获取到图层
        var Wfeaturelayer_Pic_High = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High); //获取到图层
        var Wfeaturelayer_Pic_High_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High_Text); //获取到图层
        var Wfeaturelayer_Pic_Low = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low); //获取到图层
        var Wfeaturelayer_Pic_Low_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text); //获取到图层

        if (Wfeaturelayer) {
            map.removeLayer(Wfeaturelayer); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High) {
            map.removeLayer(Wfeaturelayer_Pic_High); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low) {
            map.removeLayer(Wfeaturelayer_Pic_Low); //移除当前图层
        }

        if (Wfeaturelayer_Pic_High_Text) {
            map.removeLayer(Wfeaturelayer_Pic_High_Text); //移除当前图层
        }
        if (Wfeaturelayer_Pic_Low_Text) {
            map.removeLayer(Wfeaturelayer_Pic_Low_Text); //移除当前图层
        }

        //, minScale: 577790.55 
        //opacity=0 Known values: 0.0 - 1.0 Default value: 1.0
        var graphicLayer = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_High = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_High, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_Low = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_Low, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_High_Text = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_High_Text, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol
        var graphicLayer_Pic_Low_Text = new esri.layers.GraphicsLayer({ id: MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text, visible: false }); //定义GraphicsLayer 设置图层id为MarkerSymbol


        map.addLayers([graphicLayer, graphicLayer_Pic_High, graphicLayer_Pic_Low, graphicLayer_Pic_High_Text, graphicLayer_Pic_Low_Text]);
        var features = [];
        dojo.forEach(items, function (item) {
            var attr = {};
            attr["pname"] = item.ename; //描述内容
            attr["areaname"] = item.areaname; //所在的区名
            attr["subid"] = item.subid ? item.subid : "没有查询到数据"; //标题
            attr["stcode"] = item.stcode; //行政区划编号
            attr["type"] = item.type;
            attr["lever"] = item.lever;
            attr["tempid"] = item.tempid;
            var attr_text = {};
            attr_text["pname"] = item.ename; //描述内容
            attr_text["areaname"] = item.areaname; //所在的区名
            attr_text["subid"] = item.subid + "_Text";
            attr_text["stcode"] = item.stcode; //行政区划编号
            attr_text["type"] = item.type;
            attr_text["lever"] = item.lever;
            attr_text["tempid"] = item.tempid;


            var point = new esri.geometry.Point(parseFloat(item.longitude), parseFloat(item.latitude)); //创建点
            //var pictureMarkerSymbol = GraphicMarkImg.GetPictureMarkerSymbol(item.type, item.lever.value); // new esri.symbol.PictureMarkerSymbol(imgPath, imgWidth, imgHeight); //！！！pwat.gif
            var pictureMarkerSymbol = GraphicMarkImg.GetPictureMarkerSymbol_New(item.lever.value);

            var graphic = new esri.Graphic(point, pictureMarkerSymbol); //画布
            graphic.setAttributes(attr); //设置画布属性

            graphicLayer.add(graphic);

            var font = new esri.symbol.Font().setSize("8pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);

            //对超标倍数高的在地图上显示
            if (item.lever.value > GraphicMarkImg.st_low) {
                var textSymbol = new esri.symbol.TextSymbol(Math.round(item.lever.pollItemValue), font, new esri.Color([255, 255, 255, 0.8])).setOffset(0, 9);
                
                var graphic_text = new esri.Graphic(point, textSymbol);
                graphic_text.setAttributes(attr_text); //设置画布属性

                //graphicLayer.add(graphic_text);

                //graphicLayer_Pic_High_Text.add(graphic_text);

                //graphicLayer_Pic_Low.add(graphic);
                graphicLayer_Pic_High_Text.add(graphic_text);
                graphicLayer_Pic_High.add(graphic);
            }
            else {
                var textSymbol = new esri.symbol.TextSymbol(Math.round(item.lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(0, -8);
                
                var graphic_text = new esri.Graphic(point, textSymbol);
                graphic_text.setAttributes(attr_text); //设置画布属性
                graphicLayer.add(graphic_text);
                //
                //graphicLayer_Pic_Low_Text.add(graphic_text);
                graphicLayer_Pic_Low.add(graphic);
            }
        });

        graphicLayer_Pic_Low.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var areaname = evt.graphic.attributes.areaname; //所在的区名
            var subid = evt.graphic.attributes.subid;
            //var stcode = evt.graphic.attributes.stcode;
            //var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever;

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + "--" + areaname, font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));

            // var MarkerSymbol_High = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High); //根据FeatureLayer的ID获取到该图层
            // var MarkerSymbol_Low = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low); //根据FeatureLayer的ID获取到该图层
            //
            // map.reorderLayer(MarkerSymbol_Low, 13);  //调整图层的顺序
            // map.reorderLayer(MarkerSymbol_High, 15);

            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);


            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_High.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            //var stcode = evt.graphic.attributes.stcode;
            //var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever;

            //var r = Math.floor(Math.random() * 250);
            //var g = Math.floor(Math.random() * 100);
            //var b = Math.floor(Math.random() * 100);
            //var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            //var textSymbol = new esri.symbol.TextSymbol(pname + ": " + subid, font, new esri.Color([r, g, b, 0.8])).setOffset(5, 15);
            //

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + ", " + lever.pollItemName + "，" + Math.round(lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_High_Text.on("mouse-over", function (evt) {
            map.setMapCursor("pointer"); //添加手势

            if (map.infoWindow.isShowing) return;

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            //var stcode = evt.graphic.attributes.stcode;
            //var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever;
            //var r = Math.floor(Math.random() * 250);
            //var g = Math.floor(Math.random() * 100);
            //var b = Math.floor(Math.random() * 100);
            //var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            //var textSymbol = new esri.symbol.TextSymbol(pname + ": " + subid, font, new esri.Color([r, g, b, 0.8])).setOffset(5, 15);
            //

            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var font = new esri.symbol.Font().setSize("12pt").setWeight(esri.symbol.Font.WEIGHT_BOLD);
            var textSymbol = new esri.symbol.TextSymbol(pname + ", " + lever.pollItemName + "，" + Math.round(lever.pollItemValue), font, new esri.Color([115, 25, 60, 0.8])).setOffset(50, 30);
            map.graphics.add(new esri.Graphic(evt.graphic.geometry, textSymbol));
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever.value > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_Low.on("click", function (evt) {

            //var screenPoint = MovePointPosition_Click(evt.screenPoint);

            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=normal&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';

            //var content = "hello world";

            setTimeout(function () {
                //map.infoWindow.setContent(content);
                //map.infoWindow.setTitle("站点详情");
                //map.infoWindow.resize(390, 340); //设置infoWindow 窗体的大小
                //map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置  ANCHOR_LOWERLEFT
                //InfoWindowOnMap.SetInfowTitleClass();

                InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });
        graphicLayer_Pic_High.on("click", function (evt) {
            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=high&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';


            setTimeout(function () {
                //map.infoWindow.setContent(content);
                //map.infoWindow.setTitle("站点详情");
                //map.infoWindow.resize(390, 340); //设置infoWindow 窗体的大小
                //map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置
                //InfoWindowOnMap.SetInfowTitleClass();

                InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });
        graphicLayer_Pic_High_Text.on("click", function (evt) {
            var pname = evt.graphic.attributes.pname;
            var subid = evt.graphic.attributes.subid;
            var stcode = evt.graphic.attributes.stcode;
            var type = evt.graphic.attributes.type;
            var lever = evt.graphic.attributes.lever.value;
            var tempid = evt.graphic.attributes.tempid;
            if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) {
                subid = subid.substring(0, subid.indexOf('_'));
            }

            var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/InfoWindow_New.aspx?ty=high&StrCode=' + stcode + "&Tempid=" + tempid + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="355" height="325" frameborder="0" scrolling="no"></iframe>';


            setTimeout(function () {
                //map.infoWindow.setContent(content);
                //map.infoWindow.setTitle("站点详情");
                //map.infoWindow.resize(390, 340); //设置infoWindow 窗体的大小
                //map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置
                //InfoWindowOnMap.SetInfowTitleClass();

                InfoWindowOnMap.ShowInfoWindowOnMapByScreenPoint("", content, 355, 340, evt);
                InfoWindowOnMap.SetInfowTitleClass();
            }, 100);

            MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, "");
            //MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, "");
            if (lever > GraphicMarkImg.st_low) {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_High, subid);
            }
            else {
                MarkerSymbolLayer.HighlightSymbolNoLocation_BySuid(MarkerSymbolLayer.MarkerSymbol_Pic_Low, subid);
            }
        });

        graphicLayer_Pic_Low.on("mouse-out", function (evt) {

            map.graphics.clear();
            map.setMapCursor("default");

        });
        graphicLayer_Pic_High.on("mouse-out", function (evt) {

            map.graphics.clear();
            map.setMapCursor("default");

        });

        //设置图标的显示与隐藏
        MarkerSymbolLayer.MarkerSymbolVisibleOnMap();

        //Add by mercy@20150418pm 进行停产、故障、正常、超标个数统计
        //StatisticsEnterpriseTool.StatisticsEnterpriseCount(response.items, objcode);
    },

    RequestFailed: function (error) {
        alert('获取数据失败！无法在地图上添加标注');
        SetLoadingImg("progressBar", "", "", "", "", "hide");
    },
    MarkerSymbolVisibleOnMap: function () {
        map.graphics.clear();

        var _mapScale = map.getScale();
        var _cityLayer = map.getLayer("MarkeSymbol_CityLayer"); //3000000 visible MarkeSymbol_TextSymbolLayer

        var MarkerSymbol_High = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High); //根据FeatureLayer的ID获取到该图层
        var MarkerSymbol_Low = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low); //根据FeatureLayer的ID获取到该图层
        var MarkerSymbol_High_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_High_Text); //根据FeatureLayer的ID获取到该图层
        //var MarkerSymbol_Low_Text = map.getLayer(MarkerSymbolLayer.MarkerSymbol_Pic_Low_Text); //根据FeatureLayer的ID获取到该图层
        var countyLayer = map.getLayer("HightBorderSymbol"); //获取到图层

        //var _layerIndex_high = MarkerSymbolLayer.GetLayerIndex(MarkerSymbol_High);
        //var _layerIndex_high_text = MarkerSymbolLayer.GetLayerIndex(MarkerSymbol_High_Text);

        //排除图层没完全加载时错误加载
        if (MarkerSymbol_High == undefined || MarkerSymbol_Low == undefined || MarkerSymbol_High_Text == undefined) {
            return;
        }

        if (_mapScale < 3000000) {
            if (_cityLayer) {
                _cityLayer.hide();
            }
            MarkerSymbol_Low.show();
            MarkerSymbol_High.show();
            MarkerSymbol_High_Text.show();

            if (countyLayer) {
                map.reorderLayer(countyLayer, 10);  //调整图层的顺序
                map.reorderLayer(_cityLayer, 12);  //调整图层的顺序
            }
            map.reorderLayer(MarkerSymbol_Low, 13);  //调整图层的顺序
            map.reorderLayer(MarkerSymbol_High, 14);
            map.reorderLayer(MarkerSymbol_High_Text, 15);

            if (_mapScale < 1500000) {
                MarkerSymbol_Low.show();
                //MarkerSymbol_Low_Text.show();
            }
            else {
                //MarkerSymbol_Low.hide();
                //MarkerSymbol_Low_Text.hide();
            }
            StatisticsEnterpriseTool.ShowLegend(); //显示图例
        }
        else {
            MarkerSymbol_High.hide();
            MarkerSymbol_Low.hide();
            MarkerSymbol_High_Text.hide();
            if (_cityLayer) {
                map.reorderLayer(_cityLayer, 12);  //调整图层的顺序
                _cityLayer.show();
            }
            //_cityLayer.show();
            InfoWindowOnMap.CloseInfoWindow(); //Add by mercy@20150423pm
            StatisticsEnterpriseTool.HideLegend(); //Add by mercy@20150428 隐藏图例显示
        }

    },
    GetLayerIndex: function (layer) {
        var _index = 0;
        var _arraryIDs = map.graphicsLayerIds;
        for (var i = 0; i < _arraryIDs.length; i++) {
            if (_arraryIDs[i] == layer.id) {
                _index = i;
                break;
            }
        }
        return _index;
    },
    HighlightSymbol_BySuid: function (layerId, subid) {
        //Add by mercy@20140520在高亮显示前先关闭infowindow窗体，排除经纬度为空的情况
        if (map.infoWindow.isShowing) {
            map.infoWindow.hide();
        }

        var l = map.getLayer(layerId); //根据FeatureLayer的ID获取到该图层
        //var symbolTest = l.getSelectionSymbol(); //获取当前选中的 标注 只有FeatureLayer才有这个方法
        var graphicS = l.graphics; //获取当前图层内所有的graphic画板.可以循环这个对象 查找到要找的symbol
        var highlightSymbol = new esri.symbol.PictureMarkerSymbol(imgShinePath, imgShineWidth, imgShineHeight); //高亮标注样式 红色图片
        var pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(imgPath, imgWidth, imgHeight);
        for (var i = 0; i < graphicS.length; i++) {
            var subidAttr = graphicS[i].attributes.subid;
            if (subidAttr == subid) {
                var point = graphicS[i].geometry;
                map.centerAndZoom(point, 8);

                graphicS[i].setSymbol(highlightSymbol);

                var pname = graphicS[i].attributes.pname;
                var subid = graphicS[i].attributes.subid;
                var stcode = graphicS[i].attributes.stcode;
                var type = graphicS[i].attributes.type;

                var content = '';
                var _iframeUrl = GetUrlParaType();
                if (_iframeUrl == "AirWebGis") {
                    content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/AirQuality.aspx?StCode=' + stcode + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="330" height="200" frameborder="0" scrolling="no"></iframe>';
                }
                else {
                    content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/RealData.aspx?StCode=' + stcode + '&SubId=' + subid + '&SubName=' + escape(pname) + '&Pname=' + escape(pname) + '&remove=true" width="330" height="200" frameborder="0" scrolling="no"></iframe>';
                }

                //var content = '<iframe id="InfoWindowFrame" name="InfoWindowFrame" src="WebGis/' + type + '/RealData.aspx?StCode=' + stcode + '&SubId=' + subid + '&SubName=' + escape(pname) + '&remove=true" width="330" height="200" frameborder="0" scrolling="no"></iframe>';
                map.infoWindow.setContent(content);
                map.infoWindow.setTitle("站点详情");
                map.infoWindow.resize(350, 900); //设置infoWindow 窗体的大小
                var screenpoint = new esri.geometry.ScreenPoint(point); //经纬度坐标转屏幕坐标
                map.infoWindow.show(map.toScreen(screenpoint),
                    map.getInfoWindowAnchor(map.toScreen(screenpoint))); //infowindow在屏幕中的点的位置
                $("div[class='titleButton maximize']").addClass("hidden"); //隐藏最大化按钮
            }
            else {
                graphicS[i].setSymbol(pictureMarkerSymbol);
            }
        }
    },
    HighlightSymbolNoLocation_BySuid: function (layerId, subid) {
        if (subid.indexOf(MarkerSymbolLayer.graphic_Text) > -1) return;
        var l = map.getLayer(layerId); //根据FeatureLayer的ID获取到该图层
        var graphicS = l.graphics; //获取当前图层内所有的graphic画板.可以循环这个对象 查找到要找的symbol
        //var highlightSymbol = new esri.symbol.PictureMarkerSymbol(imgShinePath, imgShineWidth, imgShineHeight); //高亮标注样式 红色图片
        //var pictureMarkerSymbol = new esri.symbol.PictureMarkerSymbol(imgPath, imgWidth, imgHeight);
        var highlightSymbol = null;  //
        var pictureMarkerSymbol = null;

        for (var i = 0; i < graphicS.length; i++) {
            var subidAttr = graphicS[i].attributes.subid;
            if (subidAttr == subid) {
                //highlightSymbol = GraphicMarkImg.GetPictureMarkerSymbol_Falsh(graphicS[i].attributes.type, graphicS[i].attributes.lever.value);
                highlightSymbol = GraphicMarkImg.GetPictureMarkerSymbol_Falsh_New(graphicS[i].attributes.lever.value);
                graphicS[i].setSymbol(highlightSymbol);

                //map.infoWindow.setFeatures([graphicS[i]]);//infoWindow的选中形式
            }
            else {
                if (subidAttr.indexOf(MarkerSymbolLayer.graphic_Text) > -1) continue;
                //pictureMarkerSymbol = GraphicMarkImg.GetPictureMarkerSymbol(graphicS[i].attributes.type, graphicS[i].attributes.lever.value);
                pictureMarkerSymbol = GraphicMarkImg.GetPictureMarkerSymbol_New(graphicS[i].attributes.lever.value);
                graphicS[i].setSymbol(pictureMarkerSymbol);
            }
        }
        l.refresh();
    }
};

var InfoWindowOnMap = {
    SetInfowTitleClass: function () {
        if ($("div [class='title']")) {
            $("div [class='title']").removeClass("title");
        }
        $("div[class='titleButton maximize']").addClass("hidden"); //隐藏最大化按钮
        $("div[class='titlePane']").addClass("hidden"); //隐藏标题栏
        $("div[class='contentPane']").css("padding", "0px"); //设置infowindow窗体的样式
        $("div[class='contentPane']").css("overflow", "hidden");
        //$("div[class='contentPane']").css("background-color", "#4994dd");
        //$("div[class='actionsPane']").css("background-color", "#4994dd");
        //$("div[class='titlePane']").css("background-color", "#4994dd");

        //$("div[class='esriPopupWrapper']").next().css("background-color", "#beeeff");//因为是图片原因，修改颜色不起作用


        //$("div[class='esriPopup']  div").css("background-color", "transparent");
        //$("div[class='esriPopup']  div").css("background-color", "#ffffff");
        $("div[class='esriPopup']  div").css("border-style", "none");
    },
    ShowInfoWindowOnMap: function (title, content, w, h, point) {  //设置、显示地图上的infowindow窗体
        if (map.infoWindow.isShowing) {
            map.infoWindow.hide();
        }

        map.infoWindow.setContent(content);
        map.infoWindow.setTitle(); // Add by mercy@20141030am适用于infowindow窗体透明问题 map.infoWindow.setTitle(title);
        map.infoWindow.resize(w, h); //设置infoWindow 窗体的大小
        var screenpoint = new esri.geometry.ScreenPoint(point); //经纬度坐标转屏幕坐标
        map.infoWindow.show(map.toScreen(screenpoint), map.getInfoWindowAnchor(map.toScreen(screenpoint))); //infowindow在屏幕中的点的位置
        //map.infoWindow.show(map.toScreen(screenpoint), esri.dijit.InfoWindow.ANCHOR_UPPERLEFT);
        //$("div[class='titleButton maximize']").addClass("hidden"); //隐藏最大化按钮
        InfoWindowOnMap.SetInfowTitleClass();
    },
    ShowInfoWindowOnMapByScreenPoint: function (title, content, w, h, evt) {  //设置、显示地图上的infowindow窗体
        if (map.infoWindow.isShowing) {
            map.infoWindow.hide();
        }

        map.infoWindow.setContent(content);
        map.infoWindow.setTitle(); // Add by mercy@20141030am适用于infowindow窗体透明问题 map.infoWindow.setTitle(title);
        map.infoWindow.resize(w, h); //设置infoWindow 窗体的大小

        //var _anchor = esri.dijit.InfoWindow;
        map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置  ANCHOR_LOWERLEFT
        //map.infoWindow.show(evt.screenPoint, esri.dijit.InfoWindow.ANCHOR_LOWERLEFT); //infowindow在屏幕中的点的位置  ANCHOR_LOWERLEFT
        InfoWindowOnMap.SetInfowTitleClass();
    },
    ShowInfoWindowOnMapCenter: function (title, content, w, h, evt) {  //设置、显示地图上的infowindow窗体
        if (map.infoWindow.isShowing) {
            map.infoWindow.hide();
        }

        map.centerAt(evt.mapPoint);

        setTimeout(function () {
            map.infoWindow.setContent(content);
            map.infoWindow.setTitle();
            map.infoWindow.resize(w, h);

            map.infoWindow.show(evt.mapPoint);
            InfoWindowOnMap.SetInfowTitleClass();
        }, 800);
    },
    CloseInfoWindow: function () {
        if (map.infoWindow.isShowing) {
            map.infoWindow.hide();
        }
    }
};


//选中城市列表选中时的样式 Add by mercy@20140805 am
var CityListSelectedStyle = {
    DefaultSelectCity: "370100",
    SetCityListSelectedStyle: function (objid) {
        $("#city >ul >li >a").each(function (index, doElem) {
            $(doElem).removeClass("citySelected");
        });
        if (objid)
            document.getElementById(objid).className = "citySelected";
    },
    GetCityListSelected: function () {
        var _selectedId = "";
        $("#city >ul >li >a").each(function (index, doElem) {
            if ($(doElem).hasClass("citySelected")) {
                var _Id = $(doElem).attr("id");
                if (_Id.indexOf("li_") > -1) {
                    _selectedId = _Id.substr(3, _Id.length - 3);
                    return false;
                }
                //_selectedId = $(doElem).attr("id");
                //return false;
            }
        });
        if (!_selectedId) {
            _selectedId = CityListSelectedStyle.DefaultSelectCity;
        }
        return _selectedId;
    }
};



//通过Extent的方式，设置地图到全图的范围
//xmin: 12587688.256293891, ymin: 4009872.257865997, xmax: 13775140.631198635, ymax: 4625181.825218462
function SetMapToVisibleLevelByExtent() {
    if (esri.geometry && map) {
        var _extent = new esri.geometry.Extent(12587688.256293891, 4009872.257865997, 13775140.631198635, 4625181.825218462, map.spatialReference);
        map.setExtent(_extent);
    }
}


function SetStationLocation(x, y, lever) {
    $("div[class='contentPane']").fadeOut(1);
    var point = new esri.geometry.Point(parseFloat(x), parseFloat(y)); //创建点
    map.centerAndZoom(point, lever);

    setTimeout(function () {
        var screenPoint = map.toScreen(point);
        MovePointPosition(screenPoint);
        $("div[class='contentPane']").fadeIn(300);
    }, 800);
}


function MovePointPosition(pt, paramType) {
    var _iframeH = parseInt(document.getElementById("InfoWindowFrame").height); //  + 70;
    var _iframeW = parseInt(document.getElementById("InfoWindowFrame").width) + 60; //  + 20;
    var _top = Math.abs(parseInt(pt.y));
    var _bottom = Math.abs(parseInt(map.height) - parseInt(pt.y));
    var _left = Math.abs(parseInt(pt.x));
    var _right = Math.abs(parseInt(map.width) - parseInt(pt.x));
    var screenPoint = new esri.geometry.ScreenPoint(parseInt(map.width / 2), parseInt(map.height / 2));

    if (_top < _iframeH || _bottom < 100 || _left < 100 || _right < _iframeW) {

        if (_top < _iframeH) {
            if (paramType) {
                if (_top < parseInt(_iframeH / 5)) {
                    screenPoint.y = parseInt(screenPoint.y - _iframeH);
                }
                else if (_top >= parseInt(_iframeH / 5) && _top < parseInt(_iframeH / 4)) {
                    screenPoint.y = parseInt(screenPoint.y - _iframeH + 40);
                }
                else if (_top >= parseInt(_iframeH / 4) && _top < parseInt(_iframeH / 3)) {
                    screenPoint.y = parseInt(screenPoint.y - _iframeH + 60);
                }
                else if (_top >= parseInt(_iframeH / 3) && _top < parseInt(_iframeH / 2)) {
                    screenPoint.y = parseInt(screenPoint.y - _iframeH + 90);
                }
                else if (_top >= parseInt(_iframeH / 2) && _top < parseInt(_iframeH / 1.5)) {
                    screenPoint.y = parseInt(screenPoint.y - _iframeH + 150);
                }
                else if (_top >= parseInt(_iframeH / 1.5) && _top < parseInt(_iframeH / 1.2)) {
                    screenPoint.y = parseInt(screenPoint.y - parseInt(_iframeH / 2) + 20);
                }
                else {
                    screenPoint.y = parseInt(screenPoint.y - parseInt(_iframeH / 2) + 110);
                }
            }
            else {
                if (_top < parseInt(_iframeH / 2)) {
                    screenPoint.y = parseInt(screenPoint.y - _iframeH - _top);
                }
                else {
                    screenPoint.y = parseInt(screenPoint.y - parseInt(_iframeH / 3) + 50);
                }
            }
        }

        if (_bottom < 100) {
            screenPoint.y = parseInt(screenPoint.y + 70);
        }
        if (_left < 100) {
            screenPoint.x = parseInt(screenPoint.x - 90);
        }
        if (_right < _iframeW) {
            if (_right < parseInt(_iframeW / 2)) {
                screenPoint.x = parseInt(screenPoint.x + _iframeW);
            }
            else {
                screenPoint.x = parseInt(screenPoint.x + parseInt(_iframeW / 2));
                if (!paramType) {
                    screenPoint.y = parseInt(parseInt(map.height / 2) - 150);
                }
            }
        }
    }
    else {
        screenPoint = new esri.geometry.ScreenPoint(parseInt(map.width / 2), parseInt(map.height / 2));
    }
    //var screenPoint = map.toScreen(map.extent.getCenter());
    //screenPoint.spatialReference = spatialReference;  //evt.graphic.geometry.spatialReference;
    var point = map.toMap(screenPoint);
    //if (paramType) {
    //    map.centerAndZoom(point, map.getLevel());
    //}
    //else {
    //    map.centerAndZoom(point, 8);
    //}
    map.centerAndZoom(point, map.getLevel());
}

function MovePointPosition_Click(pt) {
    var _originX = pt.x;
    var _originY = pt.y;

    //var point_01 = map.toMap(pt);

    var mapCenterPoint = map.toScreen(map.extent.getCenter());
    var _offsetX = _originX + Math.abs(_originX - mapCenterPoint.x);
    var _offsetY = _originY - Math.abs(_originY - mapCenterPoint.y);

    //var screenPoint_01 = new esri.geometry.ScreenPoint(parseInt(mapCenterPoint.x + 350), parseInt(mapCenterPoint.y - 200));
    //var point_02 = map.toMap(screenPoint_01);

    var screenPoint = new esri.geometry.ScreenPoint(parseInt(_offsetX), parseInt(_offsetY));
    var point = map.toMap(screenPoint);
    map.centerAndZoom(point, map.getLevel());

    //var _infoWindowX = _originX - Math.abs(_originX - mapCenterPoint.x) - 300;
    //var _infoWindowY = _originY + Math.abs(_originY - mapCenterPoint.y) + 150;
    //var _infoWindowPoint = new esri.geometry.ScreenPoint(parseInt(_infoWindowX), parseInt(_infoWindowY));
    var _infoWindowPoint = map.toScreen(point);

    return _infoWindowPoint;
}

/*菜单样式的控制**************************************************************/
//
function SetTitleMenuListSelected() {
    $(".top_dh >ul >li").each(function (index, doElem) {
        var _src = $(this).find("img").attr("src");
        var _subSrc = "";

        $(doElem).mousemove(function () {
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src;
            }
            else {
                _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _subSrc);
        }).mouseout(function () {
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
            }
            else {
                _subSrc = _src;
            }
            $(this).find("img").attr("src", _subSrc);
        }).click(function () {
            var _srcSelect = $(this).find("img").attr("src");
            var _idSelect = $(this).find("img").attr("id");

            //重新遍历菜单按钮赋予鼠标事件并同时清除按钮的样式
            $(".top_dh >ul >li").each(function (index, doElem) {
                var _src = $(doElem).find("img").attr("src");
                var _subSrc = "";
                if (_src.indexOf("_hover") > -1) {
                    _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
                }
                else {
                    _subSrc = _src;
                }
                $(doElem).find("img").attr("src", _subSrc);

                if (index == 0) {
                    SetTitleMenuListSelected();
                }
            });

            //给单击选中的按钮添加样式，并移除当前选中按钮的鼠标滑入和滑出事件
            if (_srcSelect.indexOf("_hover") == -1) {
                _srcSelect = _srcSelect.substr(0, _srcSelect.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _srcSelect);
            $(this).unbind("mousemove").unbind("mouseout");


            GetCompany.GetCompanyList(_idSelect);
            MarkerSymbolLayer.AddMarkerSymbol("", 0, _idSelect);
            SetMapToVisibleLevelByExtent();
        });
    });
}

var type = 5;
var typepll = 1;
//不能采用each方法进行事件绑定，采用each绑定会导致多次请求，导致浏览器卡死 Add by mercy@20140815 am
$(document).ready(function () {
    var type = queryStringByName("type");
    if (type != null && type != "") {
        var strArray;
        MapSetting.isAll = "0";
        if (type.indexOf("51") == 0) {
            $("#li_WasteWaterGis").find("img").attr("src", "images/fs_hover.png");

            $("#li_WasteAirGis").find("img").attr("src", "images/fq.png");
            $("#li_SewagePlantGis").find("img").attr("src", "images/wsc.png");
            strArray = type.split("_");
            if (strArray.length > 1 && strArray[1] == "1") {
                MapSetting.isAll = "1";
                $("#daohang").hide();

                $(".top").hide();
                $("#left").css("top", "0px");
            }
        } else if (type.indexOf("52") == 0) {
            $("#li_SewagePlantGis").find("img").attr("src", "images/wsc_hover.png");

            $("#li_WasteWaterGis").find("img").attr("src", "images/fs.png");
            $("#li_WasteAirGis").find("img").attr("src", "images/fq.png");
            strArray = type.split("_");
            if (strArray.length > 1 && strArray[1] == "1") {
                MapSetting.isAll = "1";
                $("#daohang").hide();

                $(".top").hide();
                $("#left").css("top", "0px");
            }
        } else if (type.indexOf("61") == 0) {
            $("#li_WasteAirGis").find("img").attr("src", "images/fq_hover.png");

            $("#li_WasteWaterGis").find("img").attr("src", "images/fs.png");
            $("#li_SewagePlantGis").find("img").attr("src", "images/wsc.png");
            strArray = type.split("_");
            if (strArray.length > 1 && strArray[1] == "1") {
                MapSetting.isAll = "1";
                $("#daohang").hide();

                $(".top").hide();
                $("#left").css("top", "0px");
            }
        }
    }

    var height = $(window).height(), width = $(window).width();
    $("#progressBar").css("top", height / 2);
    $("#progressBar").css("right", width / 2);
    $("#progressBar").show();

    $("#li_WasteWaterGis").bind({
        mouseover: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src;
            }
            else {
                _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _subSrc);
        },
        mouseout: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
            }
            else {
                _subSrc = _src;
            }
            $(this).find("img").attr("src", _subSrc);
        },
        click: function () {
            GetCompany.isRoot = true;
            CityListSelectedStyle.SetCityListSelectedStyle("all");

            window.type = 5;
            window.typepll = 1;
            $("#li_WasteAirGis").find("img").attr("src", "images/fq.png");
            $("#li_SewagePlantGis").find("img").attr("src", "images/wsc.png");

            var _srcSelect = $(this).find("img").attr("src");
            var _idSelect = $(this).find("img").attr("id");
            //给单击选中的按钮添加样式，并移除当前选中按钮的鼠标滑入和滑出事件
            if (_srcSelect.indexOf("_hover") == -1) {
                _srcSelect = _srcSelect.substr(0, _srcSelect.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _srcSelect);
            $(this).unbind("mousemove").unbind("mouseout");
            //search();

            $("#li_WasteAirGis").bind({
                mouseover: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src;
                    }
                    else {
                        _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
                    }
                    $(this).find("img").attr("src", _subSrc);
                },
                mouseout: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
                    }
                    else {
                        _subSrc = _src;
                    }
                    $(this).find("img").attr("src", _subSrc);
                }
            });
            $("#li_SewagePlantGis").bind({
                mouseover: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src;
                    }
                    else {
                        _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
                    }
                    $(this).find("img").attr("src", _subSrc);
                },
                mouseout: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
                    }
                    else {
                        _subSrc = _src;
                    }
                    $(this).find("img").attr("src", _subSrc);
                }
            });

            // GetCompany.GetCompanyList(_idSelect);
            // MarkerSymbolLayer.AddMarkerSymbol(0, _idSelect);
            // SetMapToVisibleLevelByExtent();
            // var _selectedCity = CityListSelectedStyle.GetCityListSelected();
            // CityListSelectedStyle.SetCityListSelectedStyle(_selectedCity);
            // CityLayer.ClearCityLayer("HightBorderSymbol");

            //GetCompany.GetCompanyListCountLayer(); //Add by mercy@20150420pm 获取城市企业统计图层信息
            //var _selectedCity = CityListSelectedStyle.GetCityListSelected();


            OnClickCityHandler("all", "", "", "", "");



            //            if (_selectedCity == "all") {
            //                //CityListSelectedStyle.SetCityListSelectedStyle(_selectedCity);
            //                //CityLayer.ClearCityLayer("HightBorderSymbol");
            //                //MapSetting.SetMapToVisibleLevelByExtent();
            //                //PageLoadInit();
            //                //GetCompany.GetCompanyList(_idSelect);
            //                OnClickCityHandler("all", "", "", "", ""); //OnClickCityHandler(objid, cityName, cityCode, longitude, latitude)
            //            }
            //            else {
            //                //CityListSelectedStyle.SetCityListSelectedStyle("li_" + _selectedCity);
            //                //DoQuerieAirQuality(_selectedCity); //对选中的市描边处理
            //                //InfoWindowOnMap.CloseInfoWindow(); //关闭infowindow窗体
            //                //GetCompany.GetCompanyListByCityCode(_selectedCity);
            //                OnClickCityHandler("li_" + _selectedCity, "", _selectedCity, "", "");
            //            }


            MenuContrl.SetMenuCityHide()
            $("#arrow").attr("onclick", "");
            //MenuContrl.SetMenuCityShow()

        }
    });

    $("#li_WasteAirGis").bind({
        mouseover: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src;
            }
            else {
                _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _subSrc);
        },
        mouseout: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
            }
            else {
                _subSrc = _src;
            }
            $(this).find("img").attr("src", _subSrc);
        },
        click: function () {
            GetCompany.isRoot = true;
            CityListSelectedStyle.SetCityListSelectedStyle("all");

            window.type = 6;
            window.typepll = 1;
            $("#li_WasteWaterGis").find("img").attr("src", "images/fs.png");
            $("#li_SewagePlantGis").find("img").attr("src", "images/wsc.png");

            var _srcSelect = $(this).find("img").attr("src");
            var _idSelect = $(this).find("img").attr("id");
            //给单击选中的按钮添加样式，并移除当前选中按钮的鼠标滑入和滑出事件
            if (_srcSelect.indexOf("_hover") == -1) {
                _srcSelect = _srcSelect.substr(0, _srcSelect.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _srcSelect);
            $(this).unbind("mousemove").unbind("mouseout");

            //search();

            $("#li_WasteWaterGis").bind({
                mouseover: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src;
                    }
                    else {
                        _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
                    }
                    $(this).find("img").attr("src", _subSrc);
                },
                mouseout: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
                    }
                    else {
                        _subSrc = _src;
                    }
                    $(this).find("img").attr("src", _subSrc);
                }
            });
            $("#li_SewagePlantGis").bind({
                mouseover: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src;
                    }
                    else {
                        _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
                    }
                    $(this).find("img").attr("src", _subSrc);
                },
                mouseout: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
                    }
                    else {
                        _subSrc = _src;
                    }
                    $(this).find("img").attr("src", _subSrc);
                }
            });

            //GetCompany.GetCompanyListByCityCode(_selectedCity);

            GetCompany.GetCompanyListCountLayer(); //Add by mercy@20150420pm 获取城市企业统计图层信息
            var _selectedCity = CityListSelectedStyle.GetCityListSelected();


            OnClickCityHandler("all", "", "", "", "");

            MenuContrl.SetMenuCityHide()
            $("#arrow").attr("onclick", "");

            //MenuContrl.SetMenuCityShow();
        }
    });

    $("#li_SewagePlantGis").bind({
        mouseover: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src;
            }
            else {
                _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _subSrc);
        },
        mouseout: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
            }
            else {
                _subSrc = _src;
            }
            $(this).find("img").attr("src", _subSrc);
        },
        click: function () {
            GetCompany.isRoot = true;
            CityListSelectedStyle.SetCityListSelectedStyle("all");

            window.type = 5;
            window.typepll = 2;
            $("#li_WasteWaterGis").find("img").attr("src", "images/fs.png");
            $("#li_WasteAirGis").find("img").attr("src", "images/fq.png");

            var _srcSelect = $(this).find("img").attr("src");
            var _idSelect = $(this).find("img").attr("id");



            //给单击选中的按钮添加样式，并移除当前选中按钮的鼠标滑入和滑出事件
            if (_srcSelect.indexOf("_hover") == -1) {
                _srcSelect = _srcSelect.substr(0, _srcSelect.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _srcSelect);
            $(this).unbind("mousemove").unbind("mouseout");

            //search();
            $("#li_WasteWaterGis").bind({
                mouseover: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src;
                    }
                    else {
                        _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
                    }
                    $(this).find("img").attr("src", _subSrc);
                },
                mouseout: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
                    }
                    else {
                        _subSrc = _src;
                    }
                    $(this).find("img").attr("src", _subSrc);
                }
            });
            $("#li_WasteAirGis").bind({
                mouseover: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src;
                    }
                    else {
                        _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
                    }
                    $(this).find("img").attr("src", _subSrc);
                },
                mouseout: function () {
                    var _src = $(this).find("img").attr("src");
                    var _subSrc = "";
                    if (_src.indexOf("_hover") > -1) {
                        _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
                    }
                    else {
                        _subSrc = _src;
                    }
                    $(this).find("img").attr("src", _subSrc);
                }
            });

            // GetCompany.GetCompanyList(_idSelect);
            // MarkerSymbolLayer.AddMarkerSymbol(0, _idSelect);
            // SetMapToVisibleLevelByExtent();
            // CityListSelectedStyle.SetCityListSelectedStyle("li_370100");
            // CityLayer.ClearCityLayer("HightBorderSymbol");

            //var _selectedCity = CityListSelectedStyle.GetCityListSelected();
            //CityListSelectedStyle.SetCityListSelectedStyle("li_" + _selectedCity);
            //DoQuerieAirQuality(_selectedCity); //对选中的市描边处理
            //InfoWindowOnMap.CloseInfoWindow(); //关闭infowindow窗体
            //GetCompany.GetCompanyListByCityCode(_selectedCity);

            GetCompany.GetCompanyListCountLayer(); //Add by mercy@20150420pm 获取城市企业统计图层信息
            var _selectedCity = CityListSelectedStyle.GetCityListSelected();

            OnClickCityHandler("all", "", "", "", "");



            //            if (_selectedCity == "all") {
            //                //CityListSelectedStyle.SetCityListSelectedStyle(_selectedCity);
            //                //CityLayer.ClearCityLayer("HightBorderSymbol");
            //                //MapSetting.SetMapToVisibleLevelByExtent();
            //                // PageLoadInit();
            //                //GetCompany.GetCompanyList(_idSelect);
            //                OnClickCityHandler("all", "", "", "", ""); //OnClickCityHandler(objid, cityName, cityCode, longitude, latitude)
            //            }
            //            else {
            //                //CityListSelectedStyle.SetCityListSelectedStyle("li_" + _selectedCity);
            //                //DoQuerieAirQuality(_selectedCity); //对选中的市描边处理
            //                //InfoWindowOnMap.CloseInfoWindow(); //关闭infowindow窗体
            //                //GetCompany.GetCompanyListByCityCode(_selectedCity);
            //                OnClickCityHandler("li_" + _selectedCity, "", _selectedCity, "", "");
            //            }

            MenuContrl.SetMenuCityHide();
            $("#arrow").attr("onclick", "");

            //MenuContrl.SetMenuCityShow();
        }
    });

    $("#li_WasteWaterGis").unbind("mousemove").unbind("mouseout"); //默认祛除第一个的鼠标滑入和滑出事件



    $("#li_xxfb").bind({
        mouseover: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src;
            }
            else {
                _subSrc = _src.substr(0, _src.length - 4) + "_hover.png";
            }
            $(this).find("img").attr("src", _subSrc);
        },
        mouseout: function () {
            var _src = $(this).find("img").attr("src");
            var _subSrc = "";
            if (_src.indexOf("_hover") > -1) {
                _subSrc = _src.substring(0, _src.indexOf("_hover")) + ".png";
            }
            else {
                _subSrc = _src;
            }
            $(this).find("img").attr("src", _subSrc);
        }
    });
    ShowNotice();
});

//Add by mercy@20140801 公告通知的显示
function ShowNotice() {
    $.ajax({
        type: "get",
        url: "ajax/map.ashx?date=" + Math.random(),
        data: { Method: "GetShowNotice" },
        success: function (data) {
            if (data == "0") {
                return;
            } else {
                layer.open({
                    type: 2,
                    area: ['450px', '120px'],
                    offset: ['10px', '10px'],
                    shadeClose: false,
                    shade: 0,
                    title: false,
                    closeBtn: 2,
                    content: 'info.html'
                });
            }
        },
        error: function (data) {

            return;
        }
    });

}

//设置加载图片的位置
function SetLoadingImg(objid, left, top, right, bottom, visible) {
    if (!$("#" + objid)) return;
    if (left != "")
        $("#" + objid).css("left", parseInt(left));
    if (top != "")
        $("#" + objid).css("top", parseInt(top));
    if (right != "")
        $("#" + objid).css("right", parseInt(right));
    if (bottom != "")
        $("#" + objid).css("bottom", parseInt(bottom));

    switch (visible) {
        case "hide":
            $("#" + objid).hide();
            break;
        case "show":
            $("#" + objid).show();
            break;
        default:
            $("#" + objid).hide();
            break;
    }
}


/*添加在市区定位时，描绘市区的边框***********/

//查询地图服务值，实现定位城市边框高亮显示
function DoQuerieAirQuality(objCode) {
    //定义查询条件
    var RiverFill_QueryTask = new esri.tasks.QueryTask(MapSetting.MapQueryUrlForCity);
    var FillQuery = new esri.tasks.Query();
    FillQuery.outFields = ["CITYCODE"]; //ttribute fields to include in the FeatureSet. 所有属性字段包含在FeatureSet中
    FillQuery.returnGeometry = true;
    FillQuery.outSpatialReference = map.spatialReference; //The spatial reference for the returned geometry.

    FillQuery.where = "CITYCODE = '" + objCode + "'"; //直接用 in 只查询一次 ，一次性返回所有结果
    RiverFill_QueryTask.execute(FillQuery, function (featureSet) {
        addCountyFeatureSetToMap(featureSet, objCode);
    });
}

//添加查询到的图层信息到地图上
function addCountyFeatureSetToMap(featureSet, cityCode) {
    var symbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new esri.Color([255, 0, 0]), 3);


    //Create graphics layer for counties//为返回的结果集创建画布
    //var countyLayer = new esri.layers.GraphicsLayer();

    var countyLayer = map.getLayer("HightBorderSymbol"); //获取到图层
    if (countyLayer) {
        map.removeLayer(countyLayer); //移除当前图层
    }
    countyLayer = new esri.layers.GraphicsLayer({ id: "HightBorderSymbol" }); //定义GraphicsLayer 设置图层id为wasteLayer

    //countyLayer.setInfoTemplate(new esri.InfoTemplate("${NAME}", "${*}"));
    map.addLayer(countyLayer);

    //Add counties to the graphics layer.循环featureSet.feature
    dojo.forEach(featureSet.features, function (feature) {
        countyLayer.add(feature.setSymbol(symbol));
    });

    var _geo = featureSet.features[0].geometry.getExtent().getCenter();
    var _longitude = _geo.getLongitude();
    var _latitude = _geo.getLatitude();
    CityLayer.FlyToLocation(_longitude, _latitude, CityLayer.ZoomLever);

    var _idSelect = GetCompany.GetCurrentSelect();
    MarkerSymbolLayer.AddMarkerSymbol(cityCode, _idSelect);
}


/**************************/
//地图基本配置信息  Add by mercy@20140919 am
var MapSetting = {
    MapUrlShiLiang: "http://58.56.98.79:8003/ArcGIS/rest/services/wry20151215/MapServer",
    MapUrlYingXiang: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer",
    MapUrlDiXing: "http://58.56.98.79:8003/ArcGIS/rest/services/wry0507dx/MapServer",
    MapUrlLiuYu: "http://58.56.98.79:8003/ArcGIS/rest/services/sdriver040325/MapServer",
    MapQueryUrl: "http://58.56.98.79:8003/ArcGIS/rest/services/wry20150324/MapServer/11",  //县区所在图层
    MapQueryUrlForCity: "http://58.56.98.79:8003/ArcGIS/rest/services/wry20150324/MapServer/32", //市所在的图层
    SetMapToVisibleLevelByExtent: function () {
        if (esri.geometry && map) {
            var _extent = new esri.geometry.Extent(111.34413663606324, 32.91500980308271, 126.35146085481324, 40.15500003745771, map.spatialReference);
            map.setExtent(_extent);
        }
    },
    MapInit: function () {

    }
};


//加载矢量地图
function TiledShiLiang() {
    removeTiledLayers();
    var shiliang = new esri.layers.ArcGISTiledMapServiceLayer(MapSetting.MapUrlShiLiang, { id: "shiliang" });
    map.addLayer(shiliang);
}
//影像 
function TiledYingXiang() {
    removeTiledLayers();
    var yingxiang = new esri.layers.ArcGISTiledMapServiceLayer(MapSetting.MapUrlYingXiang, { id: "yingxiang" });
    map.addLayer(yingxiang);
}
//地形图  
function TiledDiXing() {
    removeTiledLayers();
    var dixing = new esri.layers.ArcGISTiledMapServiceLayer(MapSetting.MapUrlDiXing, { id: "dixing" });
    map.addLayer(dixing);
}
//流域 
function TiledLiuYu() {
    removeTiledLayers();
    var liuyu = new esri.layers.ArcGISTiledMapServiceLayer(MapSetting.MapUrlLiuYu, { id: "liuyu" });
    map.addLayer(liuyu);

    /***************---Add by mercy@20140616根据河流的污染等级，设置河流的颜色*********************************************/

    //var Wfeaturelayer = map.getLayer("wasteLayer"); //获取到图层
    // if (Wfeaturelayer) {
    //     map.removeLayer(Wfeaturelayer); //移除当前图层
    // }


    $.ajax({
        type: 'get',
        url: "../ajax/WaterWebGis/WaterHandler.ashx",
        data: { TabType: 'GetRiverInfo' },
        success: function (data) {
            if (data == '') return;

            $("#Hid_saveCache").val(data);
            var _riverinfo = JSON.parse(data);
            var _riverIDs = '';
            for (var i = 0; i < _riverinfo.length; i++) {
                _riverIDs += '\'' + _riverinfo[i].Subid + '\',';
            }
            if (_riverIDs == '') return;
            _riverIDs = _riverIDs.substring(0, _riverIDs.length - 1);
            doQuery(_riverIDs);
        }
    });
}


function doQuery(objIDs) {

    //查询所有河流的面
    var RiverFill_QueryTask = new esri.tasks.QueryTask(MapSetting.MapUrlLiuYu + "/1");
    //定义查询条件
    var FillQuery = new esri.tasks.Query();
    FillQuery.outFields = ["*"]; //ttribute fields to include in the FeatureSet. 所有属性字段包含在FeatureSet中
    FillQuery.returnGeometry = true;
    FillQuery.outSpatialReference = map.spatialReference; //The spatial reference for the returned geometry.
    //所有站点id的数组 
    FillQuery.where = "bm in (" + objIDs + ")"; //直接用 in 只查询一次 ，一次性返回所有结果
    RiverFill_QueryTask.execute(FillQuery, addRiverFaceFeatureSetToMap);

    //河流线的查询
    var RiverLine_QueryTask = new esri.tasks.QueryTask(MapSetting.MapUrlLiuYu + "/0");
    var LineQuery = new esri.tasks.Query();
    LineQuery.outFields = ["*"]; //ttribute fields to include in the FeatureSet. 所有属性字段包含在FeatureSet中
    LineQuery.returnGeometry = true;
    LineQuery.outSpatialReference = map.spatialReference; //The spatial reference for the returned geometry.
    LineQuery.where = "bm in (" + objIDs + ")"; //直接用 in 只查询一次 ，一次性返回所有结果
    RiverLine_QueryTask.execute(LineQuery, addRiverLineFeatureSetToMap);
}

function addRiverFaceFeatureSetToMap(featureSet) {
    var _jsonData = JSON.parse($("#Hid_saveCache").val());
    countyLayer = new esri.layers.GraphicsLayer({ id: "FaceLayer" });
    map.addLayer(countyLayer);

    dojo.forEach(featureSet.features, function (feature) {
        countyLayer.add(feature.setSymbol(SetSymbolColor(feature.attributes.bm, _jsonData)));
    });

    AttachMouseEvent(countyLayer);
}

function addRiverLineFeatureSetToMap(featureSet) {
    //Create graphics layer for counties//为返回的结果集创建画布
    var _jsonData = JSON.parse($("#Hid_saveCache").val());
    countyLayer = new esri.layers.GraphicsLayer({ id: "LineLayer" });
    map.addLayer(countyLayer);

    dojo.forEach(featureSet.features, function (feature) {
        countyLayer.add(feature.setSymbol(SetSymbolColor(feature.attributes.bm, _jsonData)));
    });

    AttachMouseEvent(countyLayer);
}

function SetSymbolColor(subID, _jsonData) {
    var _waterLevl = '';
    for (var i = 0; i < _jsonData.length; i++) {
        if (subID == _jsonData[i].Subid) {
            _waterLevl = _jsonData[i].Water;
            break;
        }
    }

    var _colorLine = '';
    var _colorFill = '';
    switch (_waterLevl) {
        case 'I':
            _colorLine = '#008700';
            _colorFill = '#00BB00';
            break;
        case 'II':
            _colorLine = '#008700';
            _colorFill = '#00BB00';
            break;
        case 'III':
            _colorLine = '#008700';
            _colorFill = '#00BB00';
            break;
        case 'IV':
            _colorLine = '#fcfc00';
            _colorFill = '#e1e100';
            break;
        case 'V':
            _colorLine = '#fea603';
            _colorFill = '#ff8000';
            break;
        case '劣V':
            _colorLine = '#ff0000';
            _colorFill = '#ea0000';
            break;
        default:
            _colorLine = '#00c5ff';
            _colorFill = '#00c5ff';
            break;
    }

    var symbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, //STYLE_SOLID 固体的 立体的
        new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, //华丽的(STYLE_DASHDOT)
            new esri.Color(_colorLine), 2), //边线的颜色 边线的宽度（宽度越到越有虚线的赶脚
        new esri.Color(_colorFill) //填充的颜色
    );
    return symbol;
}

function AttachMouseEvent(geo) {
    geo.on("mouse-over", function (evt) {
        var subID = evt.graphic.attributes.bm;

        var _jsonData = JSON.parse($("#Hid_saveCache").val());
        for (var i = 0; i < _jsonData.length; i++) {
            if (subID == _jsonData[i].Subid) {
                var _content = "<div style=' font-size:16px; width:100%;  margin:0 auto;'>";
                _content += "<table border='1' cellpadding='5' cellspacing='0' style=' margin:0 auto;'>";
                _content += "<tr><td>流域名称:</td><td>" + _jsonData[i].Basin + "</td></tr>"
                _content += "<tr><td>河流名称:</td><td>" + _jsonData[i].RiverName + "</td></tr>";
                if (_jsonData[i].RieverSection.length > 5) {
                    _content += "<tr><td>断面名称:</td><td style=' font-size:12px;'>" + _jsonData[i].RieverSection + "</td></tr>";
                }
                else {
                    _content += "<tr><td>断面名称:</td><td>" + _jsonData[i].RieverSection + "</td></tr>";
                }

                _content += "<tr><td>控制区域:</td><td>" + _jsonData[i].RiverCity + "</td></tr>";
                _content += "<tr><td>水质类别:</td><td>" + _jsonData[i].Water + "</td></tr>";
                _content += "</table></div>";
                map.infoWindow.setTitle("河流信息");
                map.infoWindow.setContent(_content);
                map.infoWindow.resize(210, 260);
                map.infoWindow.show(evt.screenPoint, map.getInfoWindowAnchor(evt.screenPoint)); //infowindow在屏幕中的点的位置

                $("div[class='titleButton maximize']").addClass("hidden"); //隐藏最大化按钮
                //$("div[class='titleButton close']").addClass("hidden");
                break;
            }
        }

    });

    geo.on("mouse-out", function (evt) {
        map.infoWindow.hide();
    });
}

/***************************************************************************/
//移除所有瓦片层
function removeTiledLayers() {
    var str = "shiliang,yingxiang,dixing,liuyu";
    var ids = map.layerIds; //获取所有图层的id的数组
    for (var i = 0; i < ids.length; i++) {
        if (str.indexOf(ids[i]) != -1) {
            var layer = map.getLayer(ids[i]); //获取到图层
            map.removeLayer(layer); //移除当前图层
        }
    }

    var _featurelayerFace = map.getLayer("FaceLayer"); //获取到图层
    var _featurelayerLine = map.getLayer("LineLayer"); //获取到图层

    if (_featurelayerFace) {
        map.removeLayer(_featurelayerFace); //移除当前图层
    }
    if (_featurelayerLine) {
        map.removeLayer(_featurelayerLine); //移除当前图层
    }
}


/**************************************/
function QH() {
    var leftDiv = document.getElementById("div_list");
    var btnImg = document.getElementById("btnImg");
    var _divSuoF = document.getElementById("suof");
    if (leftDiv.style.display == "none") {
        leftDiv.style.display = "block";
        //$("#div_list").fadeIn(500);
        btnImg.src = "images/sf2.png";
        $("#suof").css("left", "255px");
        _divSuoF.title = "关闭侧边栏";

    } else {
        leftDiv.style.display = "none";
        //$("#div_list").fadeOut(500);
        btnImg.src = "images/sf.png";
        $("#suof").css("left", "0px");
        _divSuoF.title = "打开侧边栏";
    }
}
function DHQH() {
    var leftDiv = document.getElementById("hids");
    var btnImg = document.getElementById("dh_btnImg");
    var _divDaoHang = document.getElementById("dh_sj");
    if (leftDiv.style.display == "none") {
        leftDiv.style.display = "block";
        btnImg.src = "images/dhsf2.png";
        _divDaoHang.title = "关闭导航栏";
    } else {
        leftDiv.style.display = "none";
        btnImg.src = "images/dhsf.png";
        _divDaoHang.title = "打开导航栏";
    }
}


//左侧菜单列表自动收缩
//Create by mercy@20141119pm
var MenuContrl = {
    //左侧菜单样式的控制 Add by mercy@20150416pm
    MenuCityAndEnterprise: function () {
        //        $(".city_info").mouseover(function () {
        //            $(this).css("cursor", "pointer");
        //        }).mouseout(function (ev) {
        //            var _blX = ev.clientX > 0 && ev.clientX < 300;
        //            var _blY = ev.clientY > 99 && ev.clientY < 580;
        //            if (!(_blX && _blY)) {
        //                //$(this).animate({
        //                //    width: "0px"
        //                //}).hide(500);
        //                MenuContrl.SetMenuCityHide();
        //            }
        //        });

        //        $("#city").mouseover(function () {
        //            $("#left").css("cursor", "pointer");
        //            //$(".city_info").show(200).animate({
        //            //    width: "255px"
        //            //});
        //            MenuContrl.SetMenuCityShow();
        //        });
    },
    SetMenuCityHide: function () {
        $("#left").css("width", "70px");
        $(".city_info").animate({
            //            width: "0px"
        }).hide(500);
        //        $("#left").css("width", "49px");
        //        $("#arrow").hide(500);
        //        $("#arrow").show(500);
        $("#arrows").animate({
            marginLeft: "48px",
            marginTop: "270px"
        });
        $("#arrow").attr("src", "./images/arrow_right.png");
        $("#arrow").attr("onclick", "MenuContrl.SetMenuCityShow()");
        if (MapSetting.hasOwnProperty("isAll") && MapSetting.isAll == "1") {
            $("#arrow").hide();
        }
    },
    SetMenuCityShow: function () {

        $("#left").css("width", "310px");
        $(".city_info").show(500)
        $("#arrows").animate({
            marginLeft: "310px",
            marginTop: "270px"
        });
        $("#arrow").attr("src", "./images/arrow_left.png");
        $("#arrow").attr("onclick", "MenuContrl.SetMenuCityHide()");

        $("#arrow").show();
    },
    //Add by mercy@20150416设置鹰眼样式
    SetEagleeyeStyle: function () {
        //控制鹰眼的边框样式
        $("div[class='ovwContainer']").css("background-color", "#13b5b9")
            .css("border-top-color", "#13b5b9")
            .css("border-left-color", "#13b5b9")
            .css("border-top-left-radius", "5px")
            .css("z-index", 50);
    }
};

/*
*Add by mercy@20150423pm 
*图层分析查询
*/
var GeometryAnalysis = {
    //分析
    DoGeometryQuery: function () {
        var identifyTask = new esri.tasks.IdentifyTask(MapSetting.MapUrlShiLiang);
        //IdentifyTask参数设置
        var identifyParams = new esri.tasks.IdentifyParameters();
        //冗余范围
        identifyParams.tolerance = 1;
        //返回地理元素
        identifyParams.returnGeometry = true;
        //进行Identify的图层
        identifyParams.layerIds = [32];
        //进行Identify的图层为全部
        identifyParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;
        //Identify的geometry
        identifyParams.geometry = arguments[0];
        //Identify范围
        identifyParams.mapExtent = map.extent;
        identifyTask.execute(identifyParams, GeometryAnalysis.GetSearchResultCallBack, GeometryAnalysis.ErrorCallBack);
    },
    GetSearchResultCallBack: function (results) {
        var _feature = results[0].feature.attributes;
        if (_feature)
            OnClickCityHandler("li_" + _feature.CITYCODE, "", _feature.CITYCODE, "", "");
    },
    ErrorCallBack: function (error) {
        var _vv = error;
    }
};

//
function search() {
    SetLoadingImg("progressBar", 60, 350, "", "", "show");

    var jsonData = '';
    var C0007_SUBSTATION_ID = '';
    var C0070_ENTERPRISE_NAME = $("#txt_stationName").val();
    var TYPE = window.type;
    var POLL_CODE = window.typepll;

    if (C0070_ENTERPRISE_NAME == "") {
        MarkerSymbolLayer.RequestSucceeded_Query_clear();
        SetLoadingImg("progressBar", "", "", "", "", "hide");
        document.getElementById("ss").innerHTML = "";
        return;
    }

    $.ajax({
        type: 'post',
        dataType: "json",
        url: 'ajax/map.ashx',
        data: { Method: 'GetDivByStation', C0070_ENTERPRISE_NAME: C0070_ENTERPRISE_NAME, C0007_SUBSTATION_ID: C0007_SUBSTATION_ID, SUB_TYPE: TYPE, POLL_CODE: POLL_CODE },
        success: function (data) {
            if (data == null || data.length <= 0) {
                MarkerSymbolLayer.RequestSucceeded_Query_clear();
                SetLoadingImg("progressBar", "", "", "", "", "hide");
                document.getElementById("ss").innerHTML = "";
                return;
            }
            MarkerSymbolLayer.RequestSucceeded_Query_graphic(data);
            SubHtml(C0070_ENTERPRISE_NAME, C0007_SUBSTATION_ID, data);
            SetLoadingImg("progressBar", "", "", "", "", "hide");
        },
        error: function (data) {
            MarkerSymbolLayer.RequestSucceeded_Query_clear();
            SetLoadingImg("progressBar", "", "", "", "", "hide");
            document.getElementById("ss").innerHTML = "";
            alert(data);
        }
    });
};
function SubHtml(C0070_ENTERPRISE_NAME, C0007_SUBSTATION_ID, data) {
    document.getElementById("ss").innerHTML = "";
    var html = ''
        + '<h1>查询结果 </h1>'
        + '<ul>';
    var dataObj = eval(data);
    for (var j = 0; j < dataObj.length; j++) {
        html += '<li><img src="images/enterprise.gif" width="16" height="16"><a href="javascript:void(0)" onclick="GetCompany.GetStationInfomationByID(' + dataObj[j].subid + ')">' + dataObj[j].ename + dataObj[j].subname + '</a></li>';
    }
    html += '</ul>'
    $('#ss').append(html);
}

