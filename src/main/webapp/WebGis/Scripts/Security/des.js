
/// <reference path="aes/core.js" />
/// <reference path="aes/enc-base64.js" />
/// <reference path="aes/cipher-core.js" />
/// <reference path="aes/pad-zeropadding.js" />
/// <reference path="aes/aes.js" />

var token = {
    username: 'admin',
   // key: 'ef7fce2cea944b6753b554551601d7aa4166c23cd7885eb705a49e3ad67c53a3',
    key: 'ef7fce2cea944b6753b554dssdsdssa4166c23cd7885eb705a49e3ad67c53a3',
    //pwd: '9E@@34B19E@@34B1'
    pwd: '9E@@dsdsdsdsds@@34B1 22222'
};
function encryptByAES(key1, message) {
    var key = CryptoJS.enc.Utf8.parse(key1);
    var iv = CryptoJS.enc.Utf8.parse(key1);
    var encrypted = CryptoJS.AES.encrypt(message, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.ZeroPadding
    });
    return encrypted.toString();
}

function decryptByAES(key, ciphertext) {
    var keyHex = CryptoJS.enc.Utf8.parse(key);
    var iv = CryptoJS.enc.Utf8.parse(key);
    // direct decrypt ciphertext
    var decrypted = CryptoJS.AES.decrypt(ciphertext, keyHex, { iv: iv, padding: CryptoJS.pad.ZeroPadding });
    return decrypted.toString(CryptoJS.enc.Utf8);
}