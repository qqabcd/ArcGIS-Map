/// <reference path="../../Scripts/Easyui/jquery-1.7.2.min.js" />
var tmpChart = ""; //图表数据
var xmlHttpRequest = null;


function queryStringByName(queryName) {
    var str = location.href;
    if (str.indexOf("?") > -1) {
        var queryParam = str.substring(str.indexOf("?") + 1), param = queryParam.split("&");
        if (param != null && param.length > 0) {
            for (var a = 0; a < param.length; a++) {
                var query = param[a].split("=");
                if (query[0].toUpperCase() == queryName.toUpperCase()) {
                    return query[1];
                }
            }
        }
    }
    return null;
}


//XMLHttp (创建对象)
function getXMLHttpRequest() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        var request = new ActiveXObject("Microsoft.XMLHTTP");

        if (!request) {
            request = new ActiveXObject("Msxml2.XMLHTTP");
        }

        return request;
    }

}

function GetDataInfo(url, title) {

    try {
        xmlHttpRequest = getXMLHttpRequest(); //创建xmlhttp对象
        xmlHttpRequest.open("get", url, true);

        xmlHttpRequest.onreadystatechange = function () { onReadyStateChange(title) };
        xmlHttpRequest.send(null);

    }
    catch (e) {
        alert(e);
    }
}

function CreateBar(divId, chartId, w, h) {
    document.getElementById(divId).innerHTML = "<APPLET id='" + chartId + "' archive='../chart.jar' width='" + w + "' height='" + h + "' code='com.objectplanet.chart.BarChartApplet'><param name='background' value='#CCE1FA'><param name='defaultGridLinesOn' value='true'><param name=valueLinesOn value='true'><param name=range value='100'><param name=lowerRange value='0'></APPLET>";
}

function CreateLine(divId, chartId, w, h) {
    document.getElementById(divId).innerHTML = "<APPLET id='" + chartId + "' archive='../chart.jar' width='" + w + "' height='" + h + "' code='com.objectplanet.chart.LineChartApplet'><param name='background' value='#CCE1FA'><param name='defaultGridLinesOn' value='true'><param name=valueLinesOn value='true'><param name=range value='100'><param name=lowerRange value='0'></APPLET>";
}


//单元素赋值
function SetValue(tag, tmpString) {

    if (tmpString == undefined || tmpString == "") {
        document.getElementById(tag).innerHTML = "--";
    }
    else {

        document.getElementById(tag).innerHTML = tmpString;
        if (tag == 'r_level' && tmpString == 'cr') {
            document.getElementById(tag).innerHTML = "--";
        }
    }

    // alert(tmpString);
}
//循环赋值           
function SetValues(tag, count, tmpString) {

    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById(tag + j).innerHTML = "--";
        }
    }
    else {
        var arrString = tmpString.split(",");
        //for(j=0;j<arrString.length;j++)
        for (j = 0; j < count; j++) {
            document.getElementById(tag + j).innerHTML = arrString[j];
        }
    }
}


//空气站的常态和对比情况
function SetValuesCTDB(tag, count, tmpString) {

    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById(tag + j).innerHTML = "--";
        }
    }
    else {
        var arrString = tmpString.split(",");
        //for(j=0;j<arrString.length;j++)
        for (j = 0; j < count; j++) {
            if (arrString[j] != "正常") {
                document.getElementById(tag + j).innerHTML = "<font color='red'>" + arrString[j] + "</font>";
            }
            else {
                document.getElementById(tag + j).innerHTML = arrString[j];
            }
        }
    }
}
//循环赋值           
function SetValues0(tag, count, tmpString, comString) {

    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById(tag + j).innerHTML = "--";
        }
    }
    else {
        var arrString = tmpString.split(",");
        for (j = 0; j < arrString.length; j++) {
            if (arrString[0] == comString) {
                document.getElementById(tag + 0).innerHTML = "<font color='red'>" + arrString[0] + "</font>";
            }
            else {
                document.getElementById(tag + 0).innerHTML = arrString[1];
            }
        }


    }

}


//循环赋值           
function SetValues1(tag, count, tmpString, strInit) {

    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById('R' + tag + j).style.display = "";
            document.getElementById(tag + j).innerHTML = strInit;
        }
    }
    else {
        var arrString = tmpString.split(",");
        if (arrString[0] == "停产" || arrString[0] == "在建") {
            for (j = 1; j < count; j++) {
                if (arrString[j] != "del") {
                    document.getElementById('R' + tag + j).style.display = "";
                    document.getElementById(tag + j).innerHTML = "--";
                }
                else {
                    document.getElementById('R' + tag + j).style.display = "none";
                }
            }
            document.getElementById(tag + 0).innerHTML = "<font color='red'>" + arrString[0] + "</font>";
        }
        else {
            document.getElementById(tag + 0).innerHTML = "正常";
            //第二个
            if (arrString[1] != "" && arrString[1] != "正常") {
                //1
                document.getElementById(tag + 1).innerHTML = "<font color='red'>" + arrString[1] + "</font>";

                for (j = 2; j < count; j++) {
                    if (arrString[j] == "del") {
                        document.getElementById('R' + tag + j).style.display = "none";
                    }
                    else {
                        document.getElementById('R' + tag + j).style.display = "";
                        document.getElementById(tag + j).innerHTML = "--";
                    }
                }
            }
            else {
                //1
                document.getElementById(tag + 1).innerHTML = "正常";
                for (j = 2; j < count; j++) {
                    if (arrString[j] == "del") {
                        document.getElementById('R' + tag + j).style.display = "none";
                    }
                    else if (arrString[j] == "false") { //Add by mercy@20140814有效性审核结果和时间
                        document.getElementById('R' + tag + j).style.display = "none";
                    }
                    else if (arrString[j] != "" && arrString[j] != "正常" && arrString[j] != "连续排放") {
                        document.getElementById('R' + tag + j).style.display = "";
                        document.getElementById(tag + j).innerHTML = "<font color='red'>" + arrString[j] + "</font>";
                    }
                    else {
                        document.getElementById('R' + tag + j).style.display = "";
                        document.getElementById(tag + j).innerHTML = strInit;
                    }
                }
            }
        }

    }
}


//循环赋值           
function SetValues2(tag, count, tmpString, strInit) {
    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById('R' + tag + j).style.display = "";
            document.getElementById(tag + j).innerHTML = strInit;
        }
    }
    else {
        var arrString = tmpString.split(",");
        if (arrString[count - 1] != "" && arrString[count - 1] != "正常") {
            for (j = 0; j < count - 1; j++) {
                if (arrString[j] != "del") {
                    document.getElementById('R' + tag + j).style.display = "";
                    document.getElementById(tag + j).innerHTML = "--";
                }
                else {
                    document.getElementById('R' + tag + j).style.display = "none";
                }
            }
            document.getElementById(tag + (count - 1)).innerHTML = "<font color='red'>" + arrString[count - 1] + "</font>";
        }
        else {
            for (j = 0; j < count; j++) {
                if (arrString[j] == "del") {
                    document.getElementById('R' + tag + j).style.display = "none";
                }
                else if (arrString[j] != "" && arrString[j] != "正常") {
                    document.getElementById('R' + tag + j).style.display = "";
                    document.getElementById(tag + j).innerHTML = "<font color='red'>" + arrString[j] + "</font>";
                }
                else {
                    document.getElementById('R' + tag + j).style.display = "";
                    document.getElementById(tag + j).innerHTML = strInit;
                }
            }
        }

    }

    //COD故障不显示浓度
    var stateCol = document.getElementById("runState4");
    var codCol = document.getElementById("codcm0");
    if (stateCol.innerText == "故障") {
        codCol.innerHTML = "--";
    }
}


//废水 循环赋值           
function SetValues4(tag, count, tmpString, strInit) {

    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById('R' + tag + j).style.display = "";
            document.getElementById(tag + j).innerHTML = strInit;
        }
    }
    else {
        var arrString = tmpString.split(",");
        if (arrString[0] == "停产" || arrString[0] == "在建") {
            document.getElementById(tag + 0).innerHTML = "<font color='red'>" + arrString[0] + "</font>";
            for (j = 1; j < count; j++) {
                if (arrString[j] != "del") {
                    document.getElementById('R' + tag + j).style.display = "";
                    document.getElementById(tag + j).innerHTML = "--";
                }
                else {
                    document.getElementById('R' + tag + j).style.display = "none";
                }
            }
        }
        else {
            document.getElementById(tag + 0).innerHTML = "正常"; //0

            if (arrString[count - 1] == "零排放") {
                //最后一个
                document.getElementById(tag + (count - 1)).innerHTML = "<font color='red'>" + arrString[count - 1] + "</font>";
                //第一个到倒数第二个
                for (j = 1; j < count - 1; j++) {
                    if (arrString[j] == "del") {
                        document.getElementById('R' + tag + j).style.display = "none";
                    }
                    else {
                        document.getElementById('R' + tag + j).style.display = "";
                        document.getElementById(tag + j).innerHTML = "--";
                    }
                }
            }
            else if (arrString[count - 1] != "零排放") {
                //0,最后一个
                if (arrString[count - 3] == "连续排放")
                    document.getElementById(tag + (count - 3)).innerHTML = "连续排放";
                else
                    document.getElementById(tag + (count - 3)).innerHTML = "<font color='red'>" + arrString[count - 1] + "</font>";

                //第二个
                if (arrString[1] != "" && arrString[1] != "正常") {
                    //1
                    document.getElementById(tag + 1).innerHTML = "<font color='red'>" + arrString[1] + "</font>";

                    for (j = 2; j < count - 1; j++) {
                        if (arrString[j] == "del") {
                            document.getElementById('R' + tag + j).style.display = "none";
                        }
                        else {
                            document.getElementById('R' + tag + j).style.display = "";
                            document.getElementById(tag + j).innerHTML = "--";
                        }
                    }
                }
                else {
                    //1
                    document.getElementById(tag + 1).innerHTML = "正常";
                    for (j = 2; j < count - 1; j++) {
                        if (arrString[j] == "del") {
                            document.getElementById('R' + tag + j).style.display = "none";
                        }
                        else if (arrString[j] != "" && arrString[j] != "正常" && arrString[j] != "连续排放") {
                            document.getElementById('R' + tag + j).style.display = "";
                            document.getElementById(tag + j).innerHTML = "<font color='red'>" + arrString[j] + "</font>";
                        }

                        else {
                            document.getElementById('R' + tag + j).style.display = "";
                            document.getElementById(tag + j).innerHTML = strInit;
                        }
                    }
                }

            }
        }

    }
}

//循环赋值           
function SetValuesTem(tag, count, tmpString) {
    document.getElementById(tag).style.display = "";
    var arrString = null;
    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById(tag + j).innerHTML = "--";
        }
    }
    else {
        arrString = tmpString.split(",");
        if (arrString[0] == 'del') {
            document.getElementById(tag).style.display = "none";
            arrString = null;
        }
        else {
            for (j = 0; j < arrString.length; j++) {
                document.getElementById(tag + j).innerHTML = arrString[j];
                // alert(arrString[j]);
            }
        }
    }
    return arrString;
}

//循环赋值           
function SetValuesTem1(tag, count, tmpString) {
    document.getElementById(tag).style.display = "";
    var arrString = null;
    if (tmpString == undefined || tmpString == "") {
        for (j = 0; j < count; j++) {
            document.getElementById(tag + j).innerHTML = "--";
        }
    }
    else {
        arrString = tmpString.split(",");
        if (arrString[0] == 'del') {
            document.getElementById(tag).style.display = "none";
            arrString = null;
            //alert(tag);
        }
        else {
            for (j = 0; j < 2; j++) {
                document.getElementById(tag + j).innerHTML = arrString[j];
            }
        }
    }
    return arrString;
}


//循环赋值           
function SetValues3(tag, count, tmpString, comColStr, comString, comColObj, comColTar) {
    var arrString = null;
    arrString = SetValuesTem(tag, count, tmpString);
    if (arrString != null) {
        if (arrString[comColStr] != comString && arrString[comColStr] != "--" && arrString[comColStr] != "/") {
            document.getElementById(tag + comColStr).innerHTML = "<font color='red'>" + arrString[comColStr] + "</font>";
        }

        if (parseFloat(arrString[comColObj]) > parseFloat(arrString[comColTar])) {
            document.getElementById(tag + comColObj).innerHTML = "<font color='red'>" + arrString[comColObj] + "</font>";
        }
    }

}


//循环赋值           
function SetValues5(tag, count, tmpString, comColObj0, comColObj1, comColTar) {
    //alert(tmpString);
    var arrString = null;
    arrString = SetValuesTem(tag, count, tmpString);
    if (arrString != null) {
        //alert(11);
        if (parseFloat(arrString[comColObj0]) > parseFloat(arrString[comColTar])) {
            document.getElementById(tag + comColObj0).innerHTML = "<font color='red'>" + arrString[comColObj0] + "</font>";
        }

        // if (parseFloat(arrString[comColObj1]) > parseFloat(arrString[comColTar]))
        //{
        //document.getElementById(tag + comColObj1).innerHTML = "<font color='red'>" + arrString[comColObj1] +"</font>";	
        // }
        //alert(212);   
        //var stateCol = document.getElementById("runState4");
        //var codCol = document.getElementById("codcm0");
        //if(stateCol.innerText == "故障")
        //{
        //  codCol.innerHTML = "--";
        //}


    }
}

//循环赋值           
function SetValues6(tag, count, tmpString, comColObj0, comColObj1, comColTar) {
    var arrString = null;
    arrString = SetValuesTem1(tag, count, tmpString);
    if (arrString != null) {
        if (parseFloat(arrString[comColObj0]) > parseFloat(arrString[comColTar])) {
            document.getElementById(tag + comColObj0).innerHTML = "<font color='red'>" + arrString[comColObj0] + "</font>";
        }
        /*
        if (parseFloat(arrString[comColObj1]) > parseFloat(arrString[comColTar]))
        {
        document.getElementById(tag + comColObj1).innerHTML = "<font color='red'>" + arrString[comColObj1] +"</font>";	
        }
        */
        var stateCol = document.getElementById("runState4");
        var codCol = document.getElementById("codcm0");
        if (stateCol.innerText == "故障") {
            codCol.innerHTML = "--";
        }


    }
}



/*
//循环赋值           
function SetValues5(tag,count,tmpString,comColObj0,comColObj1,comColTar)
{
var arrString = null;
arrString = SetValuesTem(tag,count,tmpString);
if (arrString != null)
{
if (parseFloat(arrString[comColObj0]) > parseFloat(arrString[comColTar]))
{
document.getElementById(tag + comColObj0).innerHTML = "<font color='red'>" + arrString[comColObj0] +"</font>";	
}
        
if (parseFloat(arrString[comColObj1]) > parseFloat(arrString[comColTar]))
{
document.getElementById(tag + comColObj1).innerHTML = "<font color='red'>" + arrString[comColObj1] +"</font>";	
}
        
        
}
}
*/





//标题赋值
function SetNonNullVal(tag, tmpString) {
    if (tmpString != undefined && tmpString != "") {
        document.getElementById(tag).innerHTML = tmpString;
    }
}


//字符数组 指定位置的值
function GetIndexValue(tmpString, splitStr, index) {
    if (tmpString == "" || tmpString == undefined) {
        return "";
    }

    var arrTemp = tmpString.split(",");
    if (arrTemp[index] != undefined) {
        return arrTemp[index];
    }
    else {
        return "";
    }

}



function clearchart(chartName)//清除曲线
{
    var objApplet = document.applets[chartName];
    objApplet.setParameter("sampleValues", ""); //X轴标签  
    objApplet.setParameter("sampleLabels", ""); //X轴标签 
    objApplet.setParameter("sampleValues_0", ""); //X轴标签  
    objApplet.setParameter("sampleValues_1", ""); //X轴标签  
    objApplet.setParameter("sampleValues_2", ""); //X轴标签  
    objApplet.setParameter("maxValueLineCount", "4"); //X轴标签  
    //objApplet.setParameter("defaultgridlineson","true,4.35,4.35"); 
    objApplet.setParameter("rangeadjusteron", "false");
    objApplet.setParameter("floatinglabelfont", "Dialog, bold, 14"); //曲线值(鼠标经过)粗体 
}



function air_subdetail(substationID) {
    window.showModalDialog("detail_air.aspx?SubID=" + substationID, "null", "resizable:1;scroll:1;dialogWidth:831px;dialogHeight:641px");
}
function river_subdetail(substationID) {
    window.showModalDialog("detail_water.aspx?SubID=" + substationID, 'null', "resizable:1;scroll:1;dialogWidth:831px;dialogHeight:663px");
}
function wa_subdetail(substationID) {
    window.showModalDialog("detail_wastegas.aspx?SubID=" + substationID, 'null', "resizable:1;scroll:1;dialogWidth:831px;dialogHeight:595px");
}
function wp_subdetail(substationID) {

    window.showModalDialog("detail_pwk.aspx?SubID=" + substationID, 'null', "resizable:1;scroll:1;dialogWidth:831px;dialogHeight:595px");
}
function sp_subdetail(substationID) {
    window.showModalDialog("detail_crk.aspx?SubID=" + substationID, 'null', "resizable:1;scroll:1;dialogWidth:831px;dialogHeight:595px");
}


//Add by mercy@20140730 格式化数据
function fmtState() {
    var isFault = false; //无故障(正常)
    var _table = document.getElementById("sbzt");
    var _trs = _table.getElementsByTagName("tr");
    var _len = _trs.length - 1;
    for (j = 0; j < _len; j++)//隐藏显示运行状态
    {
        var objDiv = document.getElementById('RrunState' + j);
        var objValue = document.getElementById('runState' + j);
        if (objDiv.style.display != "none") {
            if (objValue.innerHTML == "--" || objValue.innerHTML == "正常")//隐藏
            {
                objDiv.style.display = "none";
            }
            else//显示
            {
                if (j == (_len - 1) || j == (_len - 2)) continue;
                objDiv.style.display = "";
                isFault = true; //有故障
            }
        }
    }
    if (!isFault)//所有状态正常   
    {
        document.getElementById('RrunState0').style.display = ""; //只显示生产状态状态

        //var _tr_runState1 = "RrunState" + (_len - 2);
        //var _tr_runState2 = "RrunState" + (_len - 1);
        //document.getElementById(_tr_runState1).style.display = ""; //只显示生产状态状态 //
        //document.getElementById(_tr_runState2).style.display = ""; //只显示生产状态状态 //
    }
    //
    $("#runState" + (_len - 2) + " >font").css("color", "black");
    $("#runState" + (_len - 1) + " >font").css("color", "black");
}


//Add by merrcy@20140805获取浏览器滚动条的高度
function getScrollTop() {
    var scrollPos;
    if (window.pageYOffset) {
        scrollPos = window.pageYOffset;
    }
    else if (document.compatMode && document.compatMode != 'BackCompat') {
        scrollPos = document.documentElement.scrollTop;
    }
    else if (document.body) {
        scrollPos = document.body.scrollTop;
    }
    return scrollPos;
}

//Add by mercy@20140808 table列表隔行变色
function senfe(o, a, b, c, d) {
    var t = document.getElementById(o).getElementsByTagName("tr");
    for (var i = 0; i < t.length; i++) {
        t[i].style.backgroundColor = (t[i].sectionRowIndex % 2 == 0) ? a : b;
        //t[i].onclick = function () {
        //    if (this.x != "1") {
        //        this.x = "1"; //本来打算直接用背景色判断，FF获取到的背景是RGB值，不好判断 
        //        this.style.backgroundColor = d;
        //    } else {
        //        this.x = "0";
        //        this.style.backgroundColor = (this.sectionRowIndex % 2 == 0) ? a : b;
        //    }
        //}
        t[i].onmouseover = function () {
            if (this.x != "1") this.style.backgroundColor = c;
        }
        t[i].onmouseout = function () {
            if (this.x != "1") this.style.backgroundColor = (this.sectionRowIndex % 2 == 0) ? a : b;
        }
    }
}

//Add by mercy@20140809 显示选中的项目(柱状图和图表)
function ShowSelectItem(itemcode) {
    var _currentSelect = "";
    $(".ls_qh >ul >li").each(function (index, doElem) {
        if ($(doElem).css("background-color") != "transparent" && $(doElem).css("background-color") != "rgba(0, 0, 0, 0)") {
            _currentSelect = $(doElem).attr("id");
            return false;
        }
    });
    var _strCode = GetHiddenSaveCurrentCityCode("hid_objid");
    switch (_currentSelect) {
        case "t01":
            changShow(itemcode, _strCode);
            break;
        case "t02":
            ShowDataList(itemcode);
            break;
        default:
            changShow(itemcode, _strCode);
            break;
    }
}

//Add by mercy@20140809 设置默认选中的背景颜色(柱状图和图表)
function SetDefaultSelectedColor() {
    var _currentSelect = "";
    $(".ls_qh >ul >li").each(function (index, doElem) {
        if ($(doElem).css("background-color") != "transparent" && $(doElem).css("background-color") != "rgba(0, 0, 0, 0)") {
            _currentSelect = $(doElem).attr("id");
            return false;
        }
    });
    if (_currentSelect == "")
        $("#t01").css("background-color", "#ffad35"); //设置默认颜色 
}

//Add by mercy@20140814 站点之间切换
function ChangeRadioItem(objid, objX, objY) {
    var _stationID = objid.split('_')[1];
    SetHiddenSaveCurrentCityCode("hid_objid", _stationID); //设置站点的ID
    var _currenSelect = GetHiddenSaveCurrentCityCode("hid_ojbType");
    ChangeTabsItem(_currenSelect);

    window.parent.SetStationLocation(objX, objY, 8);
}

//保存城市的Code和当前选中的tab标签
function SetHiddenSaveCurrentCityCode(objid, strCode) {
    var _hidCode = document.getElementById(objid);
    if (_hidCode)
        _hidCode.value = strCode;
}

//获取保存在隐藏域内的值
function GetHiddenSaveCurrentCityCode(objid) {
    var _hidCode = document.getElementById(objid).value;
    if (_hidCode == "") {
        return ""; //如果隐藏域值为空，默认返回济南市
    }
    else {
        return _hidCode;
    }
}

//选项卡事件
function ChangeTabsItem(objid) {
    var _strCode = GetHiddenSaveCurrentCityCode("hid_objid");
    SetHiddenSaveCurrentCityCode("hid_ojbType", objid); //记录当前选中哪个选项卡

    switch (objid) {
        case "m01": //实时数据
            $("#m01").css("background-image", 'url("../../images/tab_hover.png")');
            $("#m02").css("background-image", 'url("../../images/tab.png")');
            $("#c01").removeClass("hidden");
            $("#c02").addClass("hidden");
            GetRealTimeData(_strCode);
            break;
        case "m02": //历史数据
            $("#m01").css("background-image", 'url("../../images/tab.png")');
            $("#m02").css("background-image", 'url("../../images/tab_hover.png")');
            $("#c01").addClass("hidden");
            $("#c02").removeClass("hidden");
            GetHistoryData(_strCode);
            break;
        case "t01": //柱状图
            $("#tt01").removeClass("hidden");
            $("#tt02").addClass("hidden");
            var _itemSelected = $("input[name='item']:checked").val();

            $("#" + objid).css("background-color", "#ffad35");
            $("#t02").css("background-color", "");
            changShow(_itemSelected, _strCode);
            break;
        case "t02": //表格图
            $("#tt01").addClass("hidden");
            $("#tt02").removeClass("hidden");
            var _itemSelected = $("input[name='item']:checked").val();

            $("#" + objid).css("background-color", "#ffad35");
            $("#t01").css("background-color", "");
            ShowDataList(_itemSelected);
            break;
    }
}

//设置标题滚动(厂区站点过多时，需要滚动查看) Add by mercy@20140818 pm
function FnScrollTitle(ojbType) {
    var _div_tile_W = $(".div_title").width();
    var _div_tile_L = $(".div_title").css("left");
    switch (ojbType) {
        case "left":
            _div_tile_L = parseInt(_div_tile_L) - 95;
            break;
        case "right":
            _div_tile_L = parseInt(_div_tile_L) + 95;
            break;
    }
    if (_div_tile_L <= 0 && Math.abs(_div_tile_L) <= (parseInt(_div_tile_W) - 300)) {
        $(".div_title").css("left", _div_tile_L);
    }
}

//Add by mercy@20140827获取当前时间
function GetCurrentTime() {
    var strDate = new Date();
    var _hour = strDate.getHours();
    var _minutes = strDate.getMinutes();
    if (_hour < 10) {
        _hour = '0' + _hour;
    }
    if (_minutes < 10) {
        _minutes = '0' + _minutes;
    }
    var _strResult = strDate.getFullYear() + '-' + (strDate.getMonth() + 1) + '-' + strDate.getDate() + ' ' + _hour + ':' + _minutes;
    return _strResult;
}

//Add by mercy@20140828 am 数组保留小数点
function ToDecimal(arrayData, len) {
    var arrayTemp = new Array();
    arrayTemp = arrayData;
    if (arrayTemp != "") {
        for (var i = 0; i < arrayTemp.length; i++) {
            if (arrayTemp[i] == "undefined" || arrayTemp[i] == "" || arrayTemp[i] == null)
                arrayTemp[i] = 0.0;
            else {
                if (len == 0) {
                    arrayTemp[i] = parseFloat(arrayTemp[i]);
                }
                else {
                    if (arrayTemp[i] < 10) {
                        arrayTemp[i] = Number(arrayTemp[i]).toFixed(2);
                    }
                    else if (arrayTemp[i] >= 10 && arrayTemp[i] < 100) {
                        arrayTemp[i] = Number(arrayTemp[i]).toFixed(len);
                    }
                    else {
                        arrayTemp[i] = parseFloat(arrayTemp[i]);
                    }
                }
            }
            //ArrNoxValues[i] = parseFloat(ArrNoxValues[i]);
        }
    }
    return arrayTemp;
}

//Add by mercy@20140901 
//一般小数位数保留原则是：整数0，小数3位；整数 1位，小数2位；整数2位，小数1位，整数三位，无小数。
//超标倍数 保留2位小数。

function ShowDataOnChartLabel(objValue) {
    var _temp = '';
    if (!objValue) {
        return "0";
    }
    objValue = objValue.toString();
    if (objValue.indexOf('.') == -1) {
        if (objValue == '0') {
            return objValue;
        }
        var _len = objValue.length;
        switch (_len) {
            case 1:
                _temp = objValue + '.00';
                break;
            case 2:
                _temp = objValue + '.0';
                break;
            default:
                _temp = objValue;
        }
    }
    else {
        var _strLen = objValue.length;
        var _index = objValue.indexOf('.');
        var _num = objValue.substr(0, _index);
        if (_num == 0) {
            switch (_strLen) {
                case 3:
                    _temp = objValue + '00';
                    break;
                case 4:
                    _temp = objValue + '0';
                    break;
                default:
                    _temp = objValue;
            }
        }
        else {
            switch (_strLen) {
                case 3:
                    _temp = objValue + '0';
                    break;
                default:
                    _temp = objValue;
            }
        }
    }
    return _temp;
}

//Add by mercy@20140901 解决曲线图的标准值的显示
function ChartTips(vv) {
    var _temp = '';
    vv = vv.toString().replace(' ', '');
    if (vv.indexOf(':') > -1) {
        var _vv = vv.substr(vv.toString().indexOf(':') + 1);
        _temp = ShowDataOnChartLabel(_vv);
    }
    else {
        _temp = vv;
    }
    _temp = "标准: " + _temp;
    return _temp;
}

//获取request参数
function GetRequest(strParame) {
    var args = new Object();
    var query = location.search.substring(1);

    var pairs = query.split("&"); // Break at ampersand
    for (var i = 0; i < pairs.length; i++) {
        var pos = pairs[i].indexOf('=');
        if (pos == -1) continue;
        var argname = pairs[i].substring(0, pos);
        var value = pairs[i].substring(pos + 1);
        //value = decodeURIComponent(value);
        value = unescape(value);
        args[argname] = value;
    }
    return args[strParame];
}

//格式化显示框中的值
function FormatValue(vv) {
    if (vv) {
        return FormatVal_New(vv);
    }
    else {
        return "--";
    }
}

function FormatValue_CBBS(vv) {
    if (vv) {
        return FormatVal_CBBS(vv);
    }
    else {
        return "--";
    }
}

//Add by mercy@20150422am 设置图标的颜色
var HighChartSetting = {
    //颜色的设置
    StatusColor: function () {
        var color = {};
        color.Normal = '#00d700';
        color.Over = '#ff311d';
        color.Stop = '#a5a5a5';
        color.Malfunction = '#decb00';
        return color;
    },
    //停产的默认值
    DefaultStopValue: 0.999999

};

//设置历史数据钮样式 Add by mercy@20150428pm
var HistoryButtonStyle = {
    ButtonMouseStyle: function (obj, oprateType, img1, img2) {
        switch (oprateType) {
            case "in":
                $(obj).attr("src", img2);
                break;
            case "out":
                $(obj).attr("src", img1);
                break;
        }
    }
};

/*+----------------------------------------------------------
*规则1     <=1：保留1位小数  如 0.1 1.0，第二位小数四舍五入
*规则2     1<x&x<100:保留3位有效数字 如  2.00
*规则3     100<=x : 保留整数，小数部分四舍五入
*规则4     1000<=x 参考规则3
* 格式化数值，规则参考以上 4条 add by liuj@20160114 15:37
+------------------------------------------------------------*/
function FormatVal_New(value) {
    var result = 0;
    try {
        if (!isNaN(value)) {
            //类型转换
            result = parseFloat(value);
            if (result <= 1) //规则1     <=1：保留1位小数  如 0.1 1.0，第二位小数四舍五入
            {
                return result.toFixed(1);
            }
            else if (1 < result && result < 100) //规则2     1<x&x<100:保留3位有效数字 如  2.00
            {
                if (result < 10) {
                    //说明是个位数
                    return result.toFixed(2);
                }
                else {
                    return result.toFixed(1);
                }
            }
            else if (100 <= result) //规则3     100<=x : 保留整数，小数部分四舍五入
            {
                return Math.round(result);
            }
        }
        else {
            return value;
        }
    } catch (e) {
        return value;
    }
    return value;
}

//格式化超标倍数数组，保留两位小时，如果值小于0.01，显示>0.01
function FormatVal_CBBS(value) {
    var result;
    try {
        if (!isNaN(value)) {
            //类型转换
            result = parseFloat(value).toFixed(2);
            if (result < 0.01) 
            {
                return "< 0.01";
            }
            else
            {
                return result;
            }
        }
        else {
            return value;
        }
    } catch (e) {
        return value;
    }
    return value;
}